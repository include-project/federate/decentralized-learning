# initialize random number generator
set.seed(0)

# generate data
# 4457 is a prime number, thus all splits will be uneven (except in 1 or in 4457)
df <- generate_dataset(n=4457, p=2)

test_data_split <- function(df, nb_split) {
    dfs <- split_data_iid(df, nb_split)

    # retrieve agent numbers
    counts <- sapply(dfs[!is.na(dfs)], nrow)
    uniq_counts <- table(counts)

    # the size of blocks should differ by at most one, so we chech that
    # 1/ there are at most two different block sizes
    expect_lte(length(uniq_counts), 2)

    # 2/ if there are two block sizes, they differ by 1
    if(length(counts) == 2)
        expect_equal(abs(uniq_counts[1] - uniq_counts[2]), 1)

    # 3/ check the sum of all parts is equal to the whole
    expect_equal(sum(counts), nrow(df))

    # 4/ there are the right number of datasets
    expect_equal(length(dfs), nb_split)
}

test_that("data splits", {
    #' split in one block
    test_data_split(df, 1)

    #' split in blocks of size one
    test_data_split(df, 4457)

    #' split in random blocks
    test_data_split(df, 324)

    #' split in too many blocks (some have none)
    test_data_split(df, 53266)

    #' check if procedure affects dataset
    df2 <- test_data_split(df, 12)
    expect_equal(all(df2 == df), FALSE)
})
