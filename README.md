# Decentralized Framework for Biostatistics

## Synopsis

This package allows setting up decentralized scientific expriments using logistic regression, with a focus on retrieving meaningful coefficients, with relevant confidence intervals.

*Disclaimer:* this package is provided as a proof of concept, proving that performing logistic regression with some basic statistics is possible without centralizing data. It does not provide any guarantee, and does not implement any data and communication security mechanisms. It must thus be used very carefully.

## Installation

The package can be installed using the following command in a R terminal:

	devtools::install_gitlab("include-project/federate/decentralized-learning")

## Example

### Full working example

A full working example can be found in [this repository](https://gitlab.com/include-project/federate/decentralized-experiments), applied to the Heart Disease Dataset (available [here](https://archive.ics.uci.edu/ml/datasets/Heart+Disease)). It reproduces results presented in a yet-to-be-published conference paper recently submitted to [STC 2020](https://stc2020.com/).

### Network computation

A decentralized logistic regression can be performed through the two following steps.

#### List other centers

Create a file (generally `hosts.txt`) of the following format:

	127.0.0.1 5001
	127.0.0.1 5002
	<remote ip> <port>
	
#### Start computation

Before starting the computation, to ensure faster convergence, the datasets should follow the following rules:

- Each local dataset should use the same set of variables.

- Data should be scaled in the same way for each center. A reasonable way of doing that without exchanging data is to divide all features by an agreed-upon value.

- All variables should be numeric variables, meaning that categorical variables should already be encoded as dummy variables, and that these variables should be the same for each center.

The computation can then be started on each agent by running

	net_reglog(dataset, port=port, hosts="hosts.txt", label="y")
	
where `dataset` is a dataframe containing the local dataset and `label` is the column to use as labels (`"y"` by default). `hosts.txt` is the file described above and `port` is the port on which to start the server.

### Algorithm simulation

For getting a grasp of what the algorithm is doing, it can also be simulated locally, running the exact same steps as in the actual decentralized setting but locally.

The dataset should be split into a list of datasets, and this list can then be used as follows

	sim_logreg(datasets)
	
where `datasets` is a list of datasets, that can be computed from on a dataset using the `split_data_iid` and `split_data_uneven` functions.
