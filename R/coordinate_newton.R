#' Coordinate Newton Method
#'
#' @param df The data to be used, including features and labels.
#' @param label The column to be used as labels. By default, the column "y" is used.
#' @param w0 Initial values of the coefficients. By default, a vectors of zero is used.
#' @param intercept If set to TRUE, compute the intercept. Otherwise assume it is zero.
#' @param nb_iter The total number of iterations to run the algortihm for.
#' @param store_coef If TRUE, returns the values of the coefficient over the run of the algorithm.
#' @param store_obj If TRUE, returns the values of the objective over the run of the algorithm.
#' @param store_only_last If TRUE, returns only last value of required return values, otherwise return all of them.
#'
#' @keywords optimization
#' @export


coordinate_newton <- function(df, label="y", w0=NULL,
                 intercept=TRUE,
                 nb_iter=100,
                 store_coef=TRUE, store_obj=TRUE, store_only_last=FALSE) {


    # initialize parameters
    prm <- initialize(df, label, w0, "MLE", intercept, NULL, nb_iter,
                      store_only_last, store_coef, store_obj)
    prm$old_coefficients <- rep(0, prm$p)
    prm$lin_values <- prm$X %*% prm$coefficients
    prm$k <- 0

    for(i in 2:(nb_iter+1)) {
        # generate random index
        k <- sample.int(prm$p, 1)# k %% p + 1

        # compute predicted values of y
        y_pred <- 1 / (1 + exp(-prm$lin_values))

        # compute the weights from the hessian diagonal
        weights <- y_pred * (1 - y_pred)

        # update model i-th coordinate
        prm$old_coefficients[k] <- prm$coefficients[k]
        prm$coefficients[k] <-
            prm$coefficients[k] + sum((prm$y - y_pred) * prm$X[,k]) / sum(weights * (prm$X[, k] ** 2))

        # update linear model X %*% w
        prm$lin_values <- prm$lin_values + (prm$coefficients[k] - prm$old_coefficients[k]) * prm$X[,k]


        update_history(prm, i,
                       store_coef, store_obj, store_only_last)
    }

    c(as.list(prm[c("X", "y", "coefficients", "history")]),
      objective=objective(prm$loss, prm$coefficients))
}
