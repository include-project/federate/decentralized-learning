#' Conjugated Gradient
#'
#' @param df The data to be used, including features and labels.
#' @param label The column to be used as labels. By default, the column "y" is used.
#' @param w0 Initial values of the coefficients. By default, a vectors of zero is used.
#' @param intercept If set to TRUE, compute the intercept. Otherwise assume it is zero.
#' @param nb_iter The total number of iterations to run the algortihm for.
#' @param store_coef If TRUE, returns the values of the coefficient over the run of the algorithm.
#' @param store_obj If TRUE, returns the values of the objective over the run of the algorithm.
#' @param store_only_last If TRUE, returns only last value of required return values, otherwise return all of them.
#'
#' @keywords optimization
#' @export


conjugate_gradient <- function(df, label="y", w0=NULL,
                               intercept=TRUE,
                               nb_iter=100,
                               store_coef=TRUE, store_obj=TRUE, store_only_last=FALSE) {

    # initialize parameters
    prm <- initialize(df, label, w0, "MLE", intercept, NULL, nb_iter,
                      store_only_last, store_coef, store_obj)
    u <- rep(1, prm$p)
    grad_old <- rep(0, prm$p)


    for(i in 2:(nb_iter+1)) {
        # compute predicted values of y
        y_pred <- 1 / (1 + exp(- prm$X %*% prm$coefficients))

        # compute the weights from the hessian diagonal
        weights <- y_pred * (1 - y_pred)

        # gradient
        grad <- t(prm$X) %*% (prm$y - y_pred)

        # update u
        beta <- t(grad) %*% (grad - grad_old) / (t(u) %*% (grad - grad_old))
        u <- grad - c(beta) * u

        # update model i-th coordinate
        prm$coefficients <-
            prm$coefficients + c(t(grad) %*% u) / sum(weights * ((prm$X %*% u)) ** 2) * u

        # store gradient
        grad_old <- grad

        update_history(prm, i,
                       store_coef, store_obj, store_only_last)
    }

    c(as.list(prm[c("X", "y", "coefficients", "history")]),
      objective=objective(prm$loss, prm$coefficients))
}
