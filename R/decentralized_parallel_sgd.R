#' Decentralized "Coordinate" Descent
#'
#' This functions solves the following optimization problem
#'     ` min f(x) `
#' where `f` is the function passed to it.
#'
#' @param df The data to be used, including features and labels.
#' @param label The column to be used as labels. By default, the column "y" is used.
#' @param w0 Initial values of the coefficients. By default, vectors of zero are used.
#' @param nb_agents Number of agents between which the data has to be split.
#' @param W Collaboration graph.
#' @param split The function to be used for splitting the data.
#' @param loss_name Name of loss function to optimize. The corresponding functions can be added in loss.R.
#' @param intercept If set to TRUE, compute the intercept. Otherwise assume it is zero.
#' @param batch The number of sample to use for computing the gradient. Uses all samples by default.
#' @param alpha Learning rate. By default, uses the optimal learning rate `1/L`.
#' @param nb_iter The number of iterations each agent runs.
#' @param store_coef If TRUE, returns the values of the coefficient over the run of the algorithm.
#' @param store_obj If TRUE, returns the values of the objective over the run of the algorithm.
#' @param store_only_last If TRUE, returns only last value of required return values, otherwise return all of them.
#'
#' @keywords optimization, gradient descent
#' @export

dec_parallel_sgd_legacy <- function(dfs, label="y", w0=NULL,
                                    mu=1, W=NULL,
                                    intercept=TRUE, batch=NULL, momentum=0,
                                    loss_name="least_squares",
                                    alpha=0.1, nb_iter=100,
                                    coef_ref=NULL, coef_loss=RMSE,
                                    random_activation=FALSE,
                                    store_coef=TRUE, store_obj=TRUE, store_only_last=FALSE) {

    # initialize parameters
    prm <- initialize_decentralized(dfs, label, W, w0, loss_name, intercept, alpha, nb_iter,
                                    random_activation,
                                    store_only_last, store_coef, store_obj, coef_ref, coef_loss)

#    pb = txtProgressBar(min = 0, max = nb_iter*prm$nb_agents, initial = 0, style=3)

    # collaboration graph
    if(is.null(W))
        W <- matrix(rep(1 / prm$nb_agents, prm$nb_agents * prm$nb_agents), ncol=prm$nb_agents)


    for(t in 1:(nb_iter*prm$nb_agents)) {
#        setTxtProgressBar(pb,t)

        # get current agent
        i <- prm$agent_activations[t]
        agent <- prm$agents[[i]]
        agent$curr_iter <- agent$curr_iter + 1

        # compute local gradient
        grad <- gradient(agent$loss, agent$w, batch)

        # compute averaged model from neighbors
        avg_w <- Reduce("+", lapply(1:prm$nb_agents, function(k) {
            W[i,k] * prm$agents[[k]]$w
        }))

        agent$w <- avg_w - alpha * grad

        # update history
        agent$history <- update_history_decentralized(prm, i, agent$curr_iter,
                                                      store_coef, store_obj, store_only_last,
                                                      coef_ref, coef_loss)

        prm$agents[[i]] <- agent

    }

    lapply(prm$agents, function(agent) {
        list(X=agent$loss$X,
             y=agent$loss$y,
             coefficients=agent$w,
             objective=objective(agent$loss, agent$w),
             history=agent$history)
    })
}
