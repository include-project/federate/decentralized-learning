
#' Split Data into chunks of given lengths.
#'
#' This function computes a repartition of a dataframe among different agents and returns it as a list.
#' The returned parts will be of lengths `lengths`.

#'
#' @param df Dataframe to split.
#' @param lengths Lengths of returned datasets.
#' @param shuffle If TRUE, shuffle the way the data is split.
#' #'
#' @keywords data handling
#' @export

split_data_from_lengths <- function(df, lengths, shuffle=TRUE) {
    n = nrow(df)

    # shuffle the data if asked
    if(shuffle)
        df <- df[sample(n),]

    # initialize list of dataframes (one for each agent)
    dfs <- list()

    pos <- 1

    for(i in 1:length(lengths)) {
        # if there are more splits than samples, just add NA at the end
        if(i <= n)
            dfs[[i]] <- df[pos:(pos + lengths[i] - 1),]
        else
            dfs[[i]] <- NA


        pos <- pos + lengths[i]
    }

    dfs
}

#' Split Data into Independently Identically Distributed Parts
#'
#' This function computes a repartition of a dataframe among different agents and returns it as a list.
#' The returned parts will be of approximate same length.

#'
#' @param df Dataframe to split.
#' @param nb_split The number of agents between the dataset must be split.
#' @param shuffle If TRUE, shuffle the way the data is split.
#' #'
#' @keywords data handling
#' @export
#'

split_data_iid <- function(df, nb_split, shuffle=TRUE) {

    # compute number of elements given to each agent
    n <- nrow(df)
    nb_bigger <- round((n / nb_split - floor(n / nb_split)) * nb_split)
    split_size <- floor(nrow(df) / nb_split)

    # vector of length
    lengths <- c(rep(split_size + 1, nb_bigger),
                 rep(split_size, nb_split - nb_bigger))

    split_data_from_lengths(df, lengths, shuffle)
}

#' Split Data into Uneven Parts
#'
#' This function computes a repartition of a dataframe among different agents and returns it as a list.
#' The returned parts will be split according to the percentage from `perc` argument.
#'
#' @param df Dataframe to split.
#' @param perc Percentages of data for each agent. The sum of these elements should be 1.
#' @param shuffle If TRUE, shuffle the way the data is split.
#' #'
#' @keywords data handling
#' @export
#'

split_data_uneven <- function(df, perc, shuffle=TRUE) {
    if(sum(perc) != 1)
        stop("Sum of proportions not equal to 1.")

    # compute number of elements given to each agent
    n <- nrow(df)

    lengths <- round(perc * n)
    lengths <- lengths[lengths > 0]

    if(sum(lengths) < n) {
        lengths[length(lengths)] = lengths[length(lengths)] + n - sum(lengths)
    }

    split_data_from_lengths(df, lengths, shuffle)
}
