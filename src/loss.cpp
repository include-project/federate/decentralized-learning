#include "loss.h"
/*
arma::colvec gradient(const arma::mat& X, const arma::colvec& y, const arma::colvec& coefficients) {
  arma::colvec grad = trans(X) * (X * coefficients - y);
  return grad / X.n_rows;
}

float objective(const arma::mat& X, const arma::colvec& y, const arma::colvec& coefficients) {
  arma::colvec residual = X * coefficients - y;
  return 1 / X.n_rows * arma::norm(residual, 2);
}
*/

float RMSE(const arma::colvec& v1, const arma::colvec& v2) {
  return 1 / sqrt((float)v1.n_elem) * arma::norm(v1 - v2, 2);
}

Loss* create_loss(const arma::mat& X, const arma::colvec& y, const std::string& name) {
  if(name == "least_squares") {
    return new LeastSquares(X, y);
  }
  else {
    return new MLE(X, y);
  }
}


arma::colvec LeastSquares::gradient(const arma::colvec& coefficients) {
  return 1.0 / _X.n_rows * trans(_X) * (_X * coefficients - _y);
}

float LeastSquares::objective(const arma::colvec& coefficients) {
  return 1.0 / _X.n_rows * pow(arma::norm(_X * coefficients - _y), 2);
}


float LeastSquares::lipschitz() {
  return 1.0 / _X.n_rows * arma::norm(trans(_X) * _X, 2);
}

arma::colvec MLE::gradient(const arma::colvec& coefficients) {
  arma::colvec Z = _X * coefficients;
  arma::colvec ones = arma::colvec(_y.n_rows, arma::fill::ones);

  return - _X.t() * (_y - ones / (ones + vecexp(-Z)));
}

float MLE::objective(const arma::colvec& coefficients) {
  arma::colvec Z = _X * coefficients;
  arma::colvec ones = arma::colvec(_y.n_rows, arma::fill::ones);

  return arma::accu(veclog(ones + vecexp(Z))) - arma::as_scalar(_y.t() * Z);
}

float MLE::lipschitz() {
  return arma::norm(trans(_X) * _X, 2);
}
