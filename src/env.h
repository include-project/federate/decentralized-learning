#pragma once

#include <vector>
#include <future>
#include <mutex>
#include <shared_mutex>

// Some forward declarations to avoid circular inclusions.
class tcp_server;
class tcp_client;


/**
 * Messages codes that will be send over the network.
 *
 * These code should be the first element of the vector sent over the network.
 */
namespace message_code {
  /// welcome message. Sent when client first connects to server.
  const int welcome = 1000;

  /// vector message. Sent by server when sending its vector.
  const int vector = 1001;

  /// ask vector message. Sent by client to ask a server for its current vector in memory.
  const int ask_vector = 1002;

  /// empty message.
  const int empty_message = 1003;

  /// heartbeat message. Used to keep a connection alive.
  const int heartbeat = 1004;

  /// covariance matrix message. Sent by server when sending its covariance matrix.
  const int cov_matrix = 1005;

  /// ask covariance matrix message. Sent by client to ask it covariance matrix.
  const int ask_cov_matrix = 1006;
}

/**
 * Some global variables that keep information about the network stuff for this program.
 */
// namespace network {
  /// pointer to the server run by this instance of the program.
//  extern tcp_server* server_pointer;

  /// pointer to the client run by this instance of the program.
//  extern tcp_client* client_pointer;

  /// initialized to false, set to true when server has finished its initialization.
//  extern bool server_ready;

  /// initialized to false, set to true when client has finished its initialization.
//  extern bool client_ready;

  /// number of clients to which the client is connected.
//  extern int nb_connected_to;

  /// number of clients connected to the server
//  extern int nb_server_session;
// }

/**
 * Some mutexes to be used when accessing shared information of server, client, or when outputting.
 */
class mtx {
 public:
  /// mutex for output. It is used in the `acout` class to claim the cout stream.
  static std::mutex cout;

  /// mutex for server variables.
  static std::mutex server;

  /// mutex for client variables.
  static std::mutex client;
};

/**
 * A fully static class regrouping all shared variables of client and server.
 */
class var {
 public:
  /**
   * Initialize global constants and variables.
   *
   * @param v_id id of this specific program.
   * @param v_nb_agents number of other agents to which this one is connected to.
   * @param v_dim dimension of the problem solved.
   */
  static void init(const int& v_id, const int& v_nb_agents, const int& v_dim) {
    // define problem constants
    id = v_id;
    dim = v_dim;
    nb_agents = v_nb_agents;

    // init first round
    init_round(0);
  }

  /**
   * (Re)initialize variables for a new round of communications.
   *
   * @param v_round number of the current round.
   */
  static void init_round(const int& v_round) {
    nb_sent = 0;
    nb_received = 0;

    serv_vector_ready = false;

    round = v_round;



    client_vec = std::vector< std::vector<float> >(nb_agents);
  }

  /// some constants identifying this program and some properties of the problem.
  static int id, nb_agents, dim;

  /// level of verbose-ness.
  static int verbose;

  /// round number (to be updated carefully by computation!)
  static int round;

  /// variables to keep track of number of send requests satisfied
  static int nb_sent;

  /// variables to keep track of number of received vectors
  static int nb_received;

  /// promise to set when vector is finished computing locally
  static bool serv_vector_ready;

  /// promise for the server vector to be sent
  static std::vector<float> serv_vec;

  /// promise for the client vectors to be received
  static std::vector< std::vector<float> > client_vec;

};
