#include "learning_rate.h"


LearningRate* create_learning_rate(const std::string& learning_rate_type,
				   Rcpp::Nullable< std::vector<float> > learning_rate_param,
				   const int& dim) {
  if(learning_rate_type == "const") {
    if(learning_rate_param.isNull()) {
      return new ConstLearningRate(-1, dim);
    }

    else {
      std::vector<float> param = Rcpp::as< std::vector<float> >(learning_rate_param);
      return new ConstLearningRate(param[0], dim);
    }
  }

  else if(learning_rate_type == "adadelta") {
    if(learning_rate_param.isNull()) {
      return new AdaDeltaLearningRate(0.9, dim, 1e-6);
    }
    else {
      std::vector<float> param = Rcpp::as< std::vector<float> >(learning_rate_param);
      return new AdaDeltaLearningRate(param[0], dim, param[1]);
    }
  }

  else {
    return 0;
  }
  
}

ConstLearningRate::ConstLearningRate(const float& learning_rate, const int& dim) {
  dim_ = dim;
  value_ = learning_rate * arma::colvec(dim, arma::fill::ones);
}

arma::colvec ConstLearningRate::get() {
  return value_;
}

void ConstLearningRate::update(Agent* agent, const int& round) {

  if(value_[0] < 0) {
    value_ = 1.0 / agent->lipschitz() * arma::colvec(agent->dim(), arma::fill::ones);
  }

}

AdaDeltaLearningRate::AdaDeltaLearningRate(const float& decay, const int& dim, const float& eps)
  : decay_(decay),
    eps_(eps) {

  dim_ = dim;
  value_ = arma::colvec(dim, arma::fill::zeros);

  acc_grad_ = arma::vec(dim, arma::fill::zeros);
  acc_update_ = arma::vec(dim, arma::fill::zeros);
}

arma::colvec AdaDeltaLearningRate::get() {
  return value_;
}

void AdaDeltaLearningRate::update(Agent* agent, const int& round) {

  arma::colvec gradient = agent->last_gradient();
  arma::colvec update = agent->last_update();
  
  // accumulate gradient
  // note that % is element-wise multiplication
  acc_grad_ = decay_ * acc_grad_ + (1 - decay_) * (gradient % gradient);
  
  // accumulate updates
  acc_update_ = decay_ * acc_update_ + (1 - decay_) * (update % update);

  value_ = arma::sqrt(acc_update_ + eps_) / arma::sqrt(acc_grad_ + eps_);
}
