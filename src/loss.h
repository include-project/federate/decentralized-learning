#pragma once
#include <cmath>
#include <string>
#include <RcppArmadillo.h>
// [[Rcpp::depends(RcppArmadillo)]]

#include "vec_exp.h"

/**
 * Compute the RMSE of two vectors, which is defined as
 *   RMSE(v,w) = 1 / \sqrt(length(v)) * \sqrt{\sum_{i=0}^length(v) (v_i - w_i)^2}.
 */
float RMSE(const arma::colvec& v1, const arma::colvec& v2);


/**
 * An abstract class that stores all elements necessary to compute objective, gradient, etc. for a specific loss that should be derived from this class.
 */
class Loss {
 public:
  /**
   * Constructor
   *
   * @param X design matrix.
   * @param y labels.
   */
 Loss(arma::mat X, arma::colvec y) : _X(X), _y(y) {};

  /**
   * Compute the gradient.
   *
   * @param coefficients coefficients to compute the gradient at.
   */
  virtual arma::colvec gradient(const arma::colvec&) = 0;


  /**
   * Compute the gradient.
   *
   * @param coefficients coefficients to compute the objective at.
   */
  virtual float objective(const arma::colvec&) = 0;

  /**
   * Compute the Lipschitz constant of the gradient for given Loss and X, y.
   */
  virtual float lipschitz() = 0;

 protected:
  /// design matrix.
  arma::mat _X;

  /// labels.
  arma::colvec _y;
};

/**
 * Loss for Least Squares:
 *   min ||Xw - y||^2.
 */
class LeastSquares: public Loss {
 public:
 LeastSquares(arma::mat X, arma::colvec y) : Loss(X, y) {};
  arma::colvec gradient(const arma::colvec&) override;
  float objective(const arma::colvec&) override;
  float lipschitz();
};

/**
 * Loss for Least Squares:
 *   max Likelihood
 */
class MLE: public Loss {
 public:
 MLE(arma::mat X, arma::colvec y) : Loss(X, y) {};
  arma::colvec gradient(const arma::colvec&) override;
  float objective(const arma::colvec&) override;
  float lipschitz();
};

/**
 * Create a new Loss and returns a pointer to it.
 *
 * @param X design matrix.
 * @param y labels.
 * @param string name of the Loss. Should be either `least_squares` or `MLE`.
 */
Loss* create_loss(const arma::mat&, const arma::colvec&, const std::string&);
