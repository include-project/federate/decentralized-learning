#include "agent.h"
#include "history.h"

Agent::Agent() {

}


Agent::Agent(Rcpp::DataFrame df,
	     std::string label,
	     std::string loss_name,
	     bool intercept,
	     arma::colvec coefficients,
	     int nb_iter, bool store_history,
	     std::string save_to,
	     std::chrono::steady_clock::time_point start,
	     int nb_history_points,
	     float cv_tol,
	     int id)

  : Agent(extract_X_from_dataframe(df, label, intercept),
	  df[label],
	  loss_name,
	  coefficients,
	  nb_iter,
	  store_history,
	  save_to,
	  start,
	  nb_history_points,
	  extract_names_from_dataframe(df, label, intercept),
	  cv_tol,
	  id) {

}

Agent::Agent(Rcpp::DataFrame df, std::string label,
	     std::string loss_name, bool intercept,
	     arma::colvec coefficients,
	     int nb_iter, bool store_history,
	     std::pair< std::ofstream*, std::ofstream* > files,
	     std::chrono::steady_clock::time_point start,
	     int nb_history_points,
	     float cv_tol,
	     int id)

  : Agent(extract_X_from_dataframe(df, label, intercept),
	  df[label],
	  loss_name,
	  coefficients,
	  nb_iter,
	  store_history,
	  files,
	  start,
	  nb_history_points,
	  extract_names_from_dataframe(df, label, intercept),
	  cv_tol,
	  id) {

}


Agent::Agent(arma::mat X, arma::vec y,
	     std::string loss_name,
	     arma::colvec coefficients,
	     int nb_iter, bool store_history,
	     std::string save_to,
	     std::chrono::steady_clock::time_point start,
	     int nb_history_points,
	     Rcpp::CharacterVector names,
	     float cv_tol,
	     int id)

  : X_(X), y_(y), names_(names),
    intercept_(false),
    store_history_(store_history), nb_history_points_(nb_history_points),
    save_path_(save_to),
    cv_tol_(cv_tol),
    loss_(0), global_loss_(0),
    coefficients_(coefficients), iter_(0),
    converged_(false),
    id_(id) {

  loss_ = create_loss(X, y, loss_name);

  history_ = history(this, nb_iter, nb_history_points_, store_history_, save_path_);
}

Agent::Agent(arma::mat X, arma::vec y,
	     std::string loss_name,
	     arma::colvec coefficients,
	     int nb_iter,
	     bool store_history,
	     std::pair< std::ofstream*, std::ofstream* > files,
	     std::chrono::steady_clock::time_point start,
	     int nb_history_points,
	     Rcpp::CharacterVector names,
	     float cv_tol,
	     int id)

  : X_(X), y_(y), names_(names),
    intercept_(false),
    store_history_(store_history), nb_history_points_(nb_history_points),
    cv_tol_(cv_tol),
    loss_(0), global_loss_(0),
    coefficients_(coefficients), iter_(0),
    converged_(false),
    id_(id) {

  loss_ = create_loss(X, y, loss_name);

  history_ = history(this, nb_iter, nb_history_points_, store_history_, files);
}

Agent::~Agent() {
}

void Agent::initialize(const int& nb_iter) {
  last_gradient_ = arma::colvec(dim(), arma::fill::zeros);
  last_update_ = arma::colvec(dim(), arma::fill::zeros);
  
  history_.reinitialize(nb_iter, nb_history_points_);
}

void Agent::initialize(const int& nb_iter,
		       std::pair< std::ofstream*, std::ofstream* > files) {
  last_gradient_ = arma::colvec(dim(), arma::fill::zeros);
  last_update_ = arma::colvec(dim(), arma::fill::zeros);
  
  history_.reinitialize(nb_iter, nb_history_points_);
}


arma::colvec Agent::gradient(const bool& store) {
  arma::colvec grad = loss_->gradient(coefficients_);

  if(store)
    last_gradient_ = grad;
  
  return grad;
}

float Agent::objective() {
  return loss_->objective(coefficients_);
}

float Agent::lipschitz() {
  return loss_->lipschitz();
}

float Agent::global_objective() {
  if(global_loss_ != 0){
    return global_loss_->objective(coefficients_);
  }
  else
    return -1;
}

void Agent::global_loss(Loss* global) {
  global_loss_ = global;
}

Rcpp::List Agent::to_list() {

  Rcpp::NumericMatrix retX = Rcpp::wrap(X_);
  colnames(retX) = names_;
  Rcpp::NumericMatrix retCoefficients = Rcpp::wrap(coefficients_);
  rownames(retCoefficients) = names_;

  return Rcpp::List::create(
    Rcpp::Named("X") = retX,
    Rcpp::Named("y") = y_,
    Rcpp::Named("coefficients") = retCoefficients,
    Rcpp::Named("objective") = loss_->objective(coefficients_),
    Rcpp::Named("converged") = converged_,
    Rcpp::Named("confint") = confint_.to_list(),
    Rcpp::Named("history") = history_.to_list());
}

void Agent::update_history() {
  history_.store();
}

arma::colvec Agent::coefficients() {
  return coefficients_;
}

void Agent::coefficients(arma::colvec new_coefficients) {
  prev_coefficients_ = coefficients_;
  coefficients_ = new_coefficients;

  last_update_ = coefficients_ - prev_coefficients_;

  if(RMSE(prev_coefficients_, coefficients_) / arma::norm(coefficients_, 2) < cv_tol_) {
    converged_ = true;
  }
}


int Agent::dim() {
  return X_.n_cols;
}

void Agent::learning_rate(float lr) {
  learning_rate_ = lr;
}


float Agent::learning_rate() {
  return learning_rate_;
}


int Agent::id() {
  return id_;
}

Rcpp::CharacterVector Agent::names() {
  return names_;
}



int Agent::iter() {
  return iter_;
}


void Agent::tick() {
  iter_++;
}

std::pair< std::ofstream*, std::ofstream* > Agent::file_handles() {
  return history_.file_handles();
}



arma::colvec Agent::last_gradient() {
  return last_gradient_;
}

arma::colvec Agent::last_update() {
  return last_update_;
}

arma::mat Agent::X() {
  return X_;
}

void Agent::confint(const ConfInt& ci) {
  confint_ = ci;
}
