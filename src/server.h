#pragma once

#define ASIO_HAS_BOOST_BIND 1

#include <iostream>
#include <string>
#include <asio.hpp>
#include <chrono>
#include <cmath>

#include <boost/bind/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>


#include "env.h"
#include "acout.h"


using asio::ip::tcp;

/**
 * A class that is instanciated each time a client connects to the server.
 */
class server_session
  : public boost::enable_shared_from_this<server_session>
{
public:
  /// shared pointer that will be shared as long as the session has work to do.
  typedef boost::shared_ptr<server_session> pointer;

  /**
   * Destructor.
   */
  ~server_session();

  /**
   * This function returns a shared pointer to a new `server_session` object.
   *
   * It should be used as a constructor.
   *
   * @param io_service the io service from asio to be used.
   * @param server server that issued this session.
   * @param port the port to start the server on.
   *
   * @return a pointer to a new `client_session` object.
   */
  static server_session::pointer create(asio::io_context& io_context, tcp_server* server, int port);

  /**
   * Return the remote socket of this session.
   *
   * @return the socket of this session.
   */
  tcp::socket& socket();

  /**
   * Start the session.
   */
  void start();

  /**
   * Asynchronously wait for something to be sent by the client.
   */
  void do_read();

  /**
   * Asynchronously write to the client.
   *
   * @param code the code corresponding to the action to be done. It depends on what the client has sent to the server before.
   */
  void do_write(const int& code);

private:
  /**
   * Contructor. Should only be called from `create` and is therefore private.
   *
   * @param io_service the io service from asio to be used.
   * @param port the port to start the server on.
   */
  server_session(asio::io_context& io_context, tcp_server* server, int port);


  /**
   * Callback after write operations.
   *
   * @param error if an error occured, it will be stored in this variable.
   * @param number_bytes_transferred number of bytes transferred during the last write operation.
   */
  void handle_write(const asio::error_code& error, size_t bytes_transferred, const int& code);


  /**
   * Callback after read operations.
   *
   * @param error if an error occured, it will be stored in this variable.
   * @param number_bytes_transferred number of bytes transferred during the last read operation.
   */
  void handle_read(const asio::error_code& error, size_t number_bytes_read);

  /**
   * Wait for a little while before trying again to send a message.
   *
   * The server runs in parallel with the computation, therefore it sometimes needs
   * to wait for the computation to end. This is done using this function, that sets
   * up a timer to wait for a given time before trying again.
   * There probably is a more usual way of waiting asynchronously for this kind of
   * computations, so it might change in the future.
   *
   * @param t time to wait.
   */
  void wait_for_round(const int& t = 100, const int& code = message_code::ask_vector);

  /**
   * Terminate the session.
   */
  void terminate();

  /// the socket used in this session.
  tcp::socket socket_;

  /// number of connections since the creation of the server.
  static int connections;

  /// id of this session.
  int connection_id_;

  /// a vector for storing the sent message.
  std::vector<float> message_;

  /// a buffer vector for receiving informations.
  std::vector<float> buffer_;

  /// timer used for waiting for end of round computation.
  asio::steady_timer round_timer_;

  /// pointer to server that created the session.
  tcp_server* server_pointer_;
};

/**
 * A class to handle all connections from clients over the network.
 */
class tcp_server
{
public:
  /**
   * Contructor.
   *
   * @param io_service the io service from asio to be used.
   * @param port the port to start the server on.
   */
  tcp_server(asio::io_context& io_context, const int& port);

  /**
   * Destructor.
   */
  ~tcp_server();

  /**
   * Stop the server.
   */
  void terminate();

  /**
   * Return number of active sessions.
   */
  int nb_active_session();

  /**
   * Count one session.
   */
  void add_session();

  /**
   * Remove one session.
   */
  void delete_session();


  /**
   * Get if client is ready.
   */
  bool ready();

  /**
   * Set ready state.
   */
  void ready(const bool& r);

private:
  /**
   * Start accepting new connections.
   *
   * This function is the idling state of the server, while it waits
   * for clients to connect. When a client connects, it creates a session
   * and starts accepting again.
   */
  void start_accept();

  /**
   * Callback of accepting function.
   *
   * @param new_connection the new server session created by the connection of a client.
   * @param error if an error occured, it will be stored in this variable.
   */
  void handle_accept(server_session::pointer new_connection,
		     const asio::error_code& error);

  /// port on which the server runs.
  int port;

  /// the io context from asio to be used.
  asio::io_context& io_context_;

  /// acceptor for new connections.
  tcp::acceptor acceptor_;

  /// parameter that is true when server needs to be stopped.
  bool stopped_;

  /// number of active sessions
  int nb_active_session_;

  /// whether server is ready.
  bool ready_;
};
