#pragma once

#define ASIO_HAS_BOOST_BIND 1

#define HEARTBEAT_DELAY 60

#include <iostream>
#include <vector>
#include <map>
#include <asio.hpp>
#include <chrono>
#include <cmath>

#include <boost/bind/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>

#include "env.h"
#include "acout.h"

using asio::ip::tcp;


/**
 * A class that is instanciated each time the client connects to a new server.
 */
class client_session : public boost::enable_shared_from_this<client_session> {
public:
  /// destructor
  ~client_session();

  /// shared pointer that will be shared as long as the session has work to do.
  typedef boost::shared_ptr<client_session> pointer;

  /**
   * This function returns a shared pointer to a new `client_session` object.
   *
   * It should be used as a constructor.
   *
   * @param io_service the io service from asio to be used.
   * @param server_id the id of the server to which this session is connected. (Used mostly for testing.)
   *
   * @return a pointer to a new `client_session` object.
   */
  static pointer create(asio::io_service& io_service, tcp_client* client, const int& server_id);

  /**
   * Return the remote socket of this session.
   *
   * @return the socket of this session.
   */
  tcp::socket& socket();

  /**
   * Start the session.
   */
  void start();

  /**
   * Asynchronously wait for something to be sent by the server.
   */
  void do_read();

  /**
   * Asynchronously wait for something to be sent by the server (first time).
   */
  void do_read_start();

  /**
   * Asynchronously write a heartbeat message to the server.
   */
  void do_heartbeat();

  /**
   * Asynchronously request the server to send the vector of current round.
   *
   * @param round the id of the round of communication this client is in.
   */
  void request_vector_from_server(const int& round, const int& message = message_code::ask_vector);

  /**
   * Terminate the session.
   */
  void terminate();


private:
  /**
   * Contructor. Should only be called from `create` and is therefore private.
   *
   * @param io_service the io service from asio to be used.
   * @param server_id the id of the server to which this session is connected. (Used mostly for testing.)
   */
  client_session(asio::io_service& io_service, tcp_client* client, const int& server_id);

  /**
   * Callback after write operations.
   *
   * @param error if an error occured, it will be stored in this variable.
   * @param number_bytes_transferred number of bytes transferred during the last write operation.
   */
  void handle_write(const asio::error_code& error, size_t number_bytes_transferred);


  /**
   * Callback after heartbeat operations.
   *
   * @param error if an error occured, it will be stored in this variable.
   * @param number_bytes_transferred number of bytes transferred during the last write operation.
   */
  void handle_heartbeat(const asio::error_code& error, size_t number_bytes_transferred);


  /**
   * Callback after read operations.
   *
   * @param error if an error occured, it will be stored in this variable.
   * @param number_bytes_transferred number of bytes transferred during the last read operation.
   */
  void handle_read(const asio::error_code& error, size_t number_bytes_read);

  /// the socket used in this session.
  tcp::socket socket_;

  /// the id of the server in this session.
  int server_id_;

  /// a bool to acknowledge whether the vector requested by `request_vector_from_server` is yet received.
  bool received_;

  /// a vector for storing the sent message.
  std::vector<float> message_;

  /// a buffer vector for receiving informations.
  std::vector<float> buffer_;

  /// the message sent for every heartbeat.
  static const std::vector<float> heartbeat_message_;

  /// the timer used for waiting before trying to reconnect if an error occured (for instance if the server is not up already.)
  asio::steady_timer heartbeat_timer_;

  /// variable that equals true if it needs to stop, false otherwise
  bool stopped_;

  /// pointer to client that created this session.
  tcp_client* client_pointer_;
};


/**
 * A class to handle all connections towards other servers.
 */
class tcp_client {
public:
  /**
   * Contructor.
   *
   * @param io_service the io service from asio to be used.
   */
  tcp_client(asio::io_service& io_service);

  /**
   * Destructor.
   */
  ~tcp_client();

  /**
   * Connect to a server. This will create a new `client_session`.
   *
   * @param endpoint the endpoint corresponding to server to connect to.
   * @param server_id id of the server to which it connects.
   */
  void connect(tcp::endpoint& endpoint, const int& server_id);

  /**
   * Reconnect to server, should only be called after `connect` has been created to create a new session.
   *
   * @param endpoint the endpoint corresponding to server to connect to.
   * @param server_id id of the server to which it connects.
   * @param new_session the new session created when first connecting. This is passed here to avoid constructing/destructing it.
   */
  void reconnect(tcp::endpoint& endpoint, const int& server_id, client_session::pointer new_session);

  /**
   * Request vectors from all servers to which the client is connected.
   *
   * @param round the id of the round of communication this client is in.
   */
  void request_vectors(const int& round, const int& message = message_code::ask_vector);


  /**
   * Terminate client.
   */
  void terminate();


  /**
   * Return number of active sessions.
   */
  int nb_active_session();

  /**
   * Count one session.
   */
  void add_session();

  /**
   * Remove one session.
   */
  void delete_session();

  /**
   * Get if client is ready.
   */
  bool ready();

  /**
   * Set ready state.
   */
  void ready(const bool& r);

private:
  /// the io service from asio to be used.
  asio::io_service& io_service_;

  /// timers for waiting before reconnection in case distant server is not available.
  std::map< int, boost::shared_ptr< asio::steady_timer > > connection_timers_;

  /// list of ongoing client sessions.
  std::map< int, client_session::pointer > connections_;

  /// number of active sessions
  int nb_active_session_;

  /// whether client is ready
  bool ready_;


  /**
   * Callback after connection operation.
   *
   * @param new_session the new session that was created.
   * @param endpoint the endpoint corresponding to the server to connect to.
   * @param server_id the id of the server to connect to.
   * @param error the error that happened during connection.
   */
  void handle_connect(client_session::pointer new_session, tcp::endpoint& endpoint, const int& server_id, const asio::error_code& error);

  /**
   * Callback after waiting before reconnection (should be called by `connection_timers_`'s timers.
   *
   * @param endpoint the endpoint corresponding to the server to connect to.
   * @param server_id the id of the server to connect to.
   * @param new_session the new session that was created.
   */
  void on_ready_to_reconnect(tcp::endpoint& endpoint, const int& server_id, client_session::pointer new_session);
};
