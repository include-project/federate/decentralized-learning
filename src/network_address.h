#pragma once

#include <fstream>
#include <asio.hpp>
#include <string>

using asio::ip::tcp;

/**
 * A class that regroups all information necessary to connect to a server.
 */
class NetworkAddress {
 public:
  /**
   * Empty constructor.
   */
  NetworkAddress() {

  }

  /**
   * Constructor.
   *
   * @param ip the ip address of server.
   * @param port the port on which it should connect.
   */
  NetworkAddress(std::string ip, float port)
    : ip_(ip),
      port_(port),
      endpoint_(tcp::endpoint(asio::ip::address::from_string(ip), port)) {
  }

  /**
   * Getter for the ip.
   */
  std::string get_ip() {
    return ip_;
  }

  /**
   * Getter for the port.
   */
  int get_port() {
    return port_;
  }

 private:
  /// ip of the server.
  std::string ip_;

  /// port to connect to.
  int port_;

  /// endpoint corresponding to ip:port.
  tcp::endpoint endpoint_;
};

/**
 * A wrapper for vector of `NetworkAddress`.
 */
class NetworkAddressList {
 public:
  /**
   * Contructor from a file.
   *
   * @param path to the file with necessary information.
   * Each line of this file should be formated as follows:
   *   127.0.0.1 1234
   */
  NetworkAddressList(std::string path) {
    from_file(path);
  }

  /**
   * Load server addresses from a file.
   *
   * @param path to the file with necessary information.
   * Each line of this file should be formated as follows:
   *   127.0.0.1 1234
   */
  void from_file(std::string path) {
    std::ifstream f;
    std::string line;
    address_.resize(0);

    f.open(path);

    while(std::getline(f, line)) {
      std::stringstream linestream(line);
      std::string ip;
      int port;

      linestream >> ip >> port;

      address_.push_back(NetworkAddress(ip, port));
    }
  }

  /**
   * Getter for the ip.
   *
   * @param i the id of the address to get the ip of.
   */
  std::string get_ip(const int& i) {
    return address_[i].get_ip();
  }


  /**
   * Getter for the port.
   *
   * @param i the id of the address to get the port of.
   */
  int get_port(const int& i) {
    return address_[i].get_port();
  }


  /**
   * Return the number of addresses stored.
   */
  int size() const {
    return address_.size();
  }

 private:
  /// the vector of stored `NetworkAddress`s.
  std::vector<NetworkAddress> address_;
};
