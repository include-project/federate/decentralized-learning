#include "algorithm.h"
#include "network.h"
#include "agent.h"

Network::Network(std::string hosts, const int& port)
  : distant_agents_(hosts) {

  nb_agents_ = distant_agents_.size();

  server_ = 0;
  client_ = 0;

  client_thread_ = new std::thread(&Network::start_client, this);
  server_thread_ = new std::thread(&Network::start_server, this, port);

  while(!ready()) {
    // wait
  }

  acout(0) << "Server ready" << std::endl;
}

Network::~Network() {
  acout(0) << "[main " << var::id << "] terminating network and joining threads" << std::endl;
  client_->terminate();
  server_->terminate();
  
  server_thread_->join();
  client_thread_->join();
}

void Network::compute_confint(Agent* agent) {

}

bool Network::ready() {
  if(server_ == 0 || client_ == 0)
    return false;

  return server_->ready()
    && client_->ready()
    && server_->nb_active_session() == nb_agents_
    && client_->nb_active_session() == nb_agents_;
}

int Network::nb_agents() {
  return nb_agents_;
}

void Network::client(tcp_client* c) {
  client_ = c;
}

void Network::server(tcp_server* s) {
  server_ = s;
}


void Network::start_server(const int& port) {

  try {
    asio::io_context io_context;

    tcp_server server(io_context, port);
    //network::server_pointer = &server;
    server_ = &server;
    server.ready(true);

    //network::server_ready = true;

    io_context.run();
  }

  catch (std::exception& e) {
    std::cerr << e.what() << std::endl;
  }
}

void Network::start_client() {

  try {
    asio::io_context io_context;

    std::vector< tcp::endpoint > endpoints(nb_agents_);

    tcp_client client(io_context);
    client_ = &client;
    //network::client_pointer = &client;

    for(int i = 0; i < nb_agents_; i++) {
      acout(2) << "sending connection to " << distant_agents_.get_ip(i) << " on port " << distant_agents_.get_port(i) << std::endl;

      endpoints[i] = tcp::endpoint(asio::ip::address::from_string(distant_agents_.get_ip(i)),
				   distant_agents_.get_port(i));
      client.connect(endpoints[i], i);
    }

    client.ready(true);
    //    network::client_ready = true;

    io_context.run();
  }

  catch (std::exception& e) {
    std::cerr << e.what() << std::endl;
  }
}

void Network::request_vectors(const int& round, const int& message) {
  client_->request_vectors(round, message);
}
