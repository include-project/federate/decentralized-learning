#pragma once

#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <chrono>
#include <utility>

#include <RcppArmadillo.h>
// [[Rcpp::depends(RcppArmadillo)]]

#include "utils.h"

class Agent;


class history {
 public:
  /**
   * Constructor.
   */
  history();


  /**
   * Contructor.
   *
   * @param agent the agent from which to store values.
   * @param nb_iter number of iteration the algorithm will be run for.
   * @param nb_stored_points number of points to store.
   * @param store if true, store in memory.
   * @param files pair of pointers to file handles to use for storing values.
   */
  history(Agent* agent,
	  const int& nb_iter,
	  const int& nb_stored_points,
	  const bool& store,
	  std::pair< std::ofstream*, std::ofstream* > files);

  /**
   * Contructor.
   *
   * @param agent the agent from which to store values.
   * @param nb_iter number of iteration the algorithm will be run for.
   * @param nb_stored_points number of points to store.
   * @param store if true, store in memory.
   * @param save_path prefix for files to use, no storing in files if empty.
   * @param init_file_handles set to true if file handles to store
   * output should be initialized.
   */
  history(Agent* agent,
	  const int& nb_iter,
	  const int& nb_stored_points,
	  const bool& store = false,
	  const std::string& save_path = "");

  /**
   * Store current value at appropriate point.
   *
   * No value will be stored if agent's current iteration is not in x_.
   */
  void store();

  /**
   * Convert to Rcpp::List to return to R.
   */
  Rcpp::Nullable< Rcpp::List > to_list();

  /**
   * Initialize relevant files.
   *
   * @param agent an agent with relevant information (number of variables, names).
   * @param prefix of files to save to.
   */
  static void initialize_files(Agent* agent, std::string save_to);



  /**
   * Initialize relevant files.
   *
   * @param agent an agent with relevant information (number of variables, names).
   * @param files files to initialize.
   */
  static void initialize_files(Agent* agent,
			       std::pair< std::ofstream*, std::ofstream* > files);

  /**
   * Reinitialize.
   */
  void reinitialize(const int& nb_iter, const int& nb_stored_points);
  
  /**
   * Get files handles
   */
  std::pair< std::ofstream*, std::ofstream* > file_handles();

 private:
  /**
   * Initialize object. Should only be called from contructor.
   *
   */
  void initialize_all(Agent* agent, const int& nb_iter, const int& nb_stored_points,
		      const bool& store, const std::string&  save_path, const bool& init_file_handles);
  
  /**
   * Store values in memory.
   *
   * @param obj current value of objective.
   * @param full_obj current value of whole objective.
   * @param time current value of time.
   * @param coefficients current value of coefficients.
   */
  void store_in_memory(float obj, float full_obj, float time, arma::rowvec coefficients);

  /**
   * Store values in files.
   *
   * @param obj current value of objective.
   * @param full_obj current value of whole objective.
   * @param time current value of time.
   * @param coefficients current value of coefficients.
   */
  void store_in_file(float obj, float full_obj, float time, arma::rowvec coefficients);

  /// Agent from which to store values.
  Agent* agent_;

  /// Whether to keep values in memory.
  bool store_;

  /// Prefix path for files where to save things.
  std::string save_path_;

  /// Files where things will be stored.
  std::ofstream* f_coef_;
  std::ofstream* f_obj_;

  /// Time at which the algorithm is started.
  std::chrono::steady_clock::time_point start_;

  /// Iterations for which values will be stored.
  std::vector<int> x_;

  /// Memory for many values.
  std::vector<float> obj_, full_obj_, rmse_, time_;

  /// Memory for coefficients.
  arma::mat coefficients_;

  /// Last index of x_ for which values were stored.
  int last_stored_point_;

  /// True if it should be saved to files.
  bool save_to_file_;
};
