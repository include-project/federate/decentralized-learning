#pragma once

#include <iostream>
#include <mutex>

#include "env.h"

/**
 * A class for handling thread safe outputs.
 *
 * This object is created at each new output, then deleted.
 */
struct acout
{
  /// A mutex that is taken whenever outputting something.
  std::unique_lock<std::mutex> lk;

  /**
   * Constructor.
   *
   * @param level the level of importance of this output. It is compared with `var::verbose` to decide whether it should be printed or not.
   */
  acout(const int& level = 1)
    : lk(std::unique_lock<std::mutex>(mtx::cout)),
      level(level) {
    // at creation, lock the mutex.

  }

  /**
   * Output operator for vectors.
   *
   * @param _v the vector to output.
   */
  acout& operator<<(std::vector<float> _v) {
    if(level <= var::verbose) {
      for(unsigned int i = 0; i < _v.size(); i++) {
	std::cout << _v[i];

	if(i != _v.size() - 1) {
	  std::cout << ", ";
	}
      }
    }

    return *this;
  }

  /**
   * Output operator.
   *
   * @tparam the type of the object to be shown.
   * @param _t the object to show.
   */
  template<typename T>
  acout& operator<<(const T& _t) {
    if(level <= var::verbose)
      std::cout << _t;
    return *this;
  }

  /**
   * Output operator for composing multiple output operators `<<`.
   *
   */
  acout& operator<<(std::ostream& (*fp)(std::ostream&)) {
    if(level <= var::verbose)
      std::cout << fp;
    return *this;
  }

  int level;
};
