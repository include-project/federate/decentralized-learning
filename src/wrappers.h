#pragma once
#include <chrono>

#include <fstream>
#include <RcppArmadillo.h>
// [[Rcpp::depends(RcppArmadillo)]]

#include <vector>
#include <string>

#include "history.h"
#include "agent.h"
#include "algorithm.h"
#include "learning_rate.h"
#include "confint.h"


//'
//' Gradient descent algorithm.
//'
//' Run gradient descent on a dataset. For now the two only loss implemented
//' are least squares and logistic (MLE) loss.
//'
//' @param df a dataframe of features and labels.
//' @param whole_df if provided, the objective on this dataset will be computed.
//' @param label the column of `df` to use as labels.
//' @param loss_name the name of the loss to use. It can be either "least_squares"
//' or 'MLE'.
//' @param intercept if set to true, fit intercept, otherwise don't.
//' @param batch the size of batches for computing gradients.
//' @param momentum value of momentum to use.
//' @param learning_rate_type type of learning rate to use.
//' @param learning_rate_param parameters for the learning rate.
//' @param nb_iter number of iterations to run the algorithm for.
//' @param cv_tol maximum variation tolerated between two iterated to decide convergence.
//' @param nb_history_points number of points to keep in memory during the run of the algorithm.
//' @param save_to prefix of path to which values will be saved as a csv file.
//' @param store_history if set to true, store all iterates, otherwise store only last.
//'
//' @export
//'
// [[Rcpp::export]]
Rcpp::List gradient_descent(Rcpp::DataFrame df,
			    Rcpp::Nullable< Rcpp::DataFrame > whole_df = R_NilValue,
			    std::string label = "y",
			    std::string loss_name = "least_squares",
			    bool intercept = true,
			    int batch = -1,
			    float momentum = 0,
			    std::string learning_rate_type = "const",
			    Rcpp::Nullable< std::vector< float > > learning_rate_param = R_NilValue,
			    int nb_iter = 10,
			    float cv_tol = 1e-6,
			    int nb_history_points = 1000,
			    std::string save_to = "",
			    bool store_history = false,
			    int id = -1,
			    bool append = false);


//'
//' Simulate decentralized stochastic gradient descent algorithm locally.
//'
//' Run decentralized gradient descent on simulated agents, each having one dataset.
//'
//' @param dfs a vector of dataframes of features and labels.
//' @param whole_df if provided, the objective on this dataset will be computed.
//' @param W the collaboration graph. If not provided, a constant graph with all
//' coefficients equal to 1/nb_agents will be used. Note that this matrix should
//' be a double stochastic matrix.
//' @param w0 starting coefficients. If not provided, 0 will be used.
//' @param label the column of `df` to use as labels.
//' @param loss_name the name of the loss to use.
//' @param intercept if set to true, fit intercept, otherwise don't.
//' @param batch the size of batches for computing gradients.
//' @param momentum value of momentum to use.
//' @param learning_rate_type type of learning rate to use.
//' @param learning_rate_param parameters for the learning rate.
//' @param decay value of the decay in ADADELTA.
//' the inverse of the Lipschitz constant of the gradient will be used.
//' @param nb_local_iter number of iterations to run between two rounds of communication.
//' @param nb_iter number of communication rounds.
//' @param cv_tol maximum variation tolerated between two iterated to decide convergence.
//' @param random_activations if set to `true`, agents will be activated
//' uniformly randomly, otherwise they will be activated in a cyclic way.
//' @param nb_history_points number of points to keep in memory during the run of the algorithm.
//' @param save_to prefix of path to which values will be saved as a csv file.
//' @param store_history if set to true, store all iterates, otherwise store only last.
//'
//' @export
//'
// [[Rcpp::export]]
Rcpp::List decentralized_gradient_descent(std::vector< Rcpp::DataFrame > dfs,
					  Rcpp::Nullable< Rcpp::DataFrame > whole_df = R_NilValue,
					  Rcpp::Nullable< Rcpp::NumericMatrix > W = R_NilValue,
					  Rcpp::Nullable< Rcpp::NumericVector > w0 = R_NilValue,
					  std::string label = "y",
					  std::string loss_name = "least_squares",
					  bool intercept = true,
					  int batch = -1,
					  float momentum = 0,
					  std::string learning_rate_type = "const",
					  Rcpp::Nullable< std::vector< float > > learning_rate_param = R_NilValue,
					  float decay = 0.9,
					  int nb_local_iter = 1,
					  int nb_iter = 10,
					  float cv_tol = 1e-6,
					  bool random_activations = true,
					  int nb_history_points = 1000,
					  std::string save_to = "",
					  bool store_history = false);

//'
//' Run decentralized stochastic gradient descent on the network.
//'
//' This function will only work if the same function is run on the servers
//' described in a file "hosts.txt" (filename to provide as argument `agents_addresses`).
//' It starts a server, connect to others, then runs the algorithms by effectively
//' communicating with other agents through internet.
//'
//' @param dfs a vector of dataframes of features and labels.
//' @param agent_addresses path to a file containing addresses and ports of other
//' agents part-taking in the computation. The file should contain one address
//' on each line formatted as
//'   127.0.0.1 1234
//' @param port the port to listen for other clients' communications on.
//' @param whole_df if provided, the objective on this dataset will be computed.
//' @param W the collaboration graph. If not provided, a constant graph with all
//' coefficients equal to 1/nb_agents will be used. Note that this matrix should
//' be a double stochastic matrix.
//' @param w0 starting coefficients. If not provided, 0 will be used.
//' @param label the column of `df` to use as labels.
//' @param loss_name the name of the loss to use.
//' @param intercept if set to true, fit intercept, otherwise don't.
//' @param batch the size of batches for computing gradients.
//' @param momentum value of momentum to use.
//' @param learning_rate_type type of learning rate to use.
//' @param learning_rate_param parameters for the learning rate.
//' @param decay value of the decay in ADADELTA.
//' the inverse of the Lipschitz constant of the gradient will be used.
//' @param nb_local_iter number of iterations to run between two rounds of communication.
//' @param nb_iter number of communication rounds.
//' @param cv_tol maximum variation tolerated between two iterated to decide convergence.
//' @param random_activations if set to `true`, agents will be activated
//' uniformly randomly, otherwise they will be activated in a cyclic way.
//' @param nb_history_points number of points to keep in memory during the run of the algorithm.
//' @param save_to prefix of path to which values will be saved as a csv file.
//' @param store_history if set to true, store all iterates, otherwise store only last.
//'
//' @export
//'
// [[Rcpp::export]]
Rcpp::List network_gradient_descent(Rcpp::DataFrame df,
				    std::string agents_addresses,
				    int port = 6767,
				    Rcpp::Nullable< Rcpp::DataFrame > whole_df = R_NilValue,
				    Rcpp::Nullable< arma::colvec > collab = R_NilValue,
				    Rcpp::Nullable< Rcpp::NumericVector > w0 = R_NilValue,
				    std::string label = "y",
				    std::string loss_name = "least_squares",
				    bool intercept = true,
				    int batch = -1,
				    float momentum = 0,
				    std::string learning_rate_type = "const",
				    Rcpp::Nullable< std::vector< float > > learning_rate_param = R_NilValue,
				    float decay = 0.9,
				    int nb_local_iter = 1,
				    int nb_iter = 10,
				    float cv_tol = 1e-6,
				    int nb_history_points = 1000,
				    std::string save_to = "",
				    bool store_history = false,
				    int id = -1,
				    bool append = false,
				    bool verbose = false);
