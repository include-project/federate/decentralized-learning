#pragma once

#include <cmath>
#include <vector>
#include <RcppArmadillo.h>
#include <boost/math/distributions/students_t.hpp>

#include "env.h"
#include "network.h"
#include "vec_exp.h"

class Agent;

class ConfInt {
 public:
  ConfInt(float confidence = 0.95);

  Rcpp::Nullable< Rcpp::List > to_list();

  void compute_on_network(Agent* agent, Network* network);
  void compute_total_nb_samples(Agent* agent, Network* network);

 private:
  bool defined_;
  float confidence_;
  int total_samples_;

  arma::colvec lower_;
  arma::colvec upper_;

  arma::mat compute_local(Agent* agent);
  arma::mat aggregate_cov(Agent* agent, const std::vector< std::vector< float > >& vec);
  arma::colvec aggregate_stderror(Agent* agent, const std::vector< std::vector< float > >& vec);
};
