#pragma once

#include <vector>
#include <thread>


#include "acout.h"
#include "client.h"
#include "server.h"
#include "env.h"
#include "network_address.h"

class Agent;
class DecentralizedAlgorithm;

class Network {
 public:
  Network(std::string hosts, const int& port);
  ~Network();

  void run_algo(DecentralizedAlgorithm* algorithm, Agent* agent);
  void compute_confint(Agent* agent);

  bool ready();
  int nb_agents();

  void client(tcp_client* c);
  void server(tcp_server* s);

  void start_client();
  void start_server(const int& port);

  void request_vectors(const int& round, const int& message = message_code::ask_vector);

 private:
  NetworkAddressList distant_agents_;
  int nb_agents_;
  tcp_client* client_;
  tcp_server* server_;

  std::thread* server_thread_;
  std::thread* client_thread_;
};
