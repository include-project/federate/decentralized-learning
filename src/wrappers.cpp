#include "wrappers.h"


Rcpp::List gradient_descent(Rcpp::DataFrame df,
			    Rcpp::Nullable< Rcpp::DataFrame > whole_df,
			    std::string label,
			    std::string loss_name,
			    bool intercept,
			    int batch,
			    float momentum,
			    std::string learning_rate_type,
			    Rcpp::Nullable< std::vector< float > > learning_rate_param,
			    int nb_iter,
			    float cv_tol,
			    int nb_history_points,
			    std::string save_to,
			    bool store_history,
			    int id,
			    bool append) {

  // generate zero coefficients
  arma::colvec coefficients(dimension(df, intercept), arma::fill::zeros);

  // start timer
  std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();

  // compute global loss
  Loss* global_loss = 0;
  if(whole_df.isNotNull()) {
    Rcpp::DataFrame wdf = Rcpp::as<Rcpp::DataFrame>(whole_df);
    global_loss = create_loss(extract_X_from_dataframe(wdf, label, intercept),
			      wdf[label],
			      loss_name);
  }

  // create learning rate
  LearningRate* learning_rate = create_learning_rate(learning_rate_type,
						     learning_rate_param,
						     dimension(df, intercept));
  
  std::ofstream f_coef, f_obj;

  if(!append && !save_to.empty()) {
    f_coef.open(save_to + "_coef.csv");
    f_obj.open(save_to + "_obj.csv");
  }
  else if(!save_to.empty()) {
    f_coef.open(save_to + "_coef.csv", std::ios::app);
    f_obj.open(save_to + "_obj.csv", std::ios::app);
  }

  // put all elements in an agent object
  Agent agent(df,
	      label,
	      loss_name,
	      intercept,
	      coefficients,
	      nb_iter,
	      store_history,
	      std::make_pair(&f_obj, &f_coef),
	      start,
	      nb_history_points,
	      cv_tol,
	      id);

  if(!save_to.empty() && !append)
    history::initialize_files(&agent, std::make_pair(&f_obj, &f_coef));


  if(global_loss != 0)
    agent.global_loss(global_loss);

  // run algorithm
  GradientDescent gd(nb_iter, learning_rate, batch);
  gd.run(&agent);


  f_coef.close();
  f_obj.close();


  // return agent formatted to R list
  return agent.to_list();
}

Rcpp::List decentralized_gradient_descent(std::vector< Rcpp::DataFrame > dfs,
					  Rcpp::Nullable< Rcpp::DataFrame > whole_df,
					  Rcpp::Nullable< Rcpp::NumericMatrix > W,
					  Rcpp::Nullable< Rcpp::NumericVector > w0,
					  std::string label,
					  std::string loss_name,
					  bool intercept,
					  int batch,
					  float momentum,
					  std::string learning_rate_type,
					  Rcpp::Nullable< std::vector< float > > learning_rate_param,
					  float decay,
					  int nb_local_iter,
					  int nb_iter,
					  float cv_tol,
					  bool random_activations,
					  int nb_history_points,
					  std::string save_to,
					  bool store_history) {

  // generate zero coefficients
  arma::colvec coefficients(dimension(dfs[0], intercept), arma::fill::zeros);

  // start timer
  std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();

  // compute global loss
  Loss* global_loss = 0;
  if(whole_df.isNotNull()) {
    Rcpp::DataFrame wdf = Rcpp::as<Rcpp::DataFrame>(whole_df);
    global_loss = create_loss(extract_X_from_dataframe(wdf, label, intercept),
			      wdf[label],
			      loss_name);
  }

  // create learning rate
  LearningRate* learning_rate = create_learning_rate(learning_rate_type,
						     learning_rate_param,
						     dimension(dfs[0], intercept));

  // create relevant agent objects
  std::vector< Agent* > agents(dfs.size());


  std::ofstream f_coef, f_obj;
  if(!save_to.empty()) {
    f_coef.open(save_to + "_coef.csv");
    f_obj.open(save_to + "_obj.csv");
  }

  for(unsigned int i = 0; i < dfs.size(); i++) {

    agents[i] = new Agent(dfs[i],
			  label,
			  loss_name,
			  intercept,
			  coefficients,
			  nb_iter,
			  store_history,
			  std::make_pair(&f_obj, &f_coef),
			  start,
			  nb_history_points,
			  cv_tol,
			  i);

    if(global_loss != 0)
      agents[i]->global_loss(global_loss);

  }

  // initialize history files if relevant
  if(!save_to.empty())
    history::initialize_files(agents[0], std::make_pair(&f_obj, &f_coef));


  // run algorithm
  DecentralizedGradientDescent dgd(nb_local_iter,
				   nb_iter,
				   learning_rate,
				   batch);

  if(W.isNotNull()) {
    arma::mat collab_graph = Rcpp::as<arma::mat>(W);
    dgd.run_locally(agents, true, collab_graph);
  }
  else
    dgd.run_locally(agents);


  f_coef.close();
  f_obj.close();

  // return agents formatted to R list
  Rcpp::List ret = Rcpp::List::create();
  for(unsigned int i = 0; i < agents.size(); i++)
    ret.push_back(agents[i]->to_list());

  return ret;

}


Rcpp::List network_gradient_descent(Rcpp::DataFrame df,
				    std::string agents_addresses,
				    int port,
				    Rcpp::Nullable< Rcpp::DataFrame > whole_df,
				    Rcpp::Nullable< arma::colvec > collab,
				    Rcpp::Nullable< Rcpp::NumericVector > w0,
				    std::string label,
				    std::string loss_name,
				    bool intercept,
				    int batch,
				    float momentum,
				    std::string learning_rate_type,
				    Rcpp::Nullable< std::vector< float > > learning_rate_param,
				    float decay,
				    int nb_local_iter,
				    int nb_iter,
				    float cv_tol,
				    int nb_history_points,
				    std::string save_to,
				    bool store_history,
				    int id,
				    bool append,
				    bool verbose) {

  // set verbose
  if(verbose)
    var::verbose = 3;
  else
    var::verbose = -1;

  // generate zero coefficients
  arma::colvec coefficients(dimension(df, intercept), arma::fill::zeros);

  // compute global loss
  Loss* global_loss = 0;
  if(whole_df.isNotNull()) {
    Rcpp::DataFrame wdf = Rcpp::as<Rcpp::DataFrame>(whole_df);
    global_loss = create_loss(extract_X_from_dataframe(wdf, label, intercept),
			      wdf[label],
			      loss_name);
  }

  
  // create learning rate
  LearningRate* learning_rate = create_learning_rate(learning_rate_type,
						     learning_rate_param,
						     dimension(df, intercept));


  // start timer
  std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();


  std::ofstream f_coef, f_obj;

  if(!append && !save_to.empty()) {
    f_coef.open(save_to + "_coef.csv");
    f_obj.open(save_to + "_obj.csv");
  }
  else if(!save_to.empty()) {
    f_coef.open(save_to + "_coef.csv", std::ios::app);
    f_obj.open(save_to + "_obj.csv", std::ios::app);
  }


  // put all elements in an agent object
  Agent agent(df,
	      label,
	      loss_name,
	      intercept,
	      coefficients,
	      nb_iter,
	      store_history,
	      std::make_pair(&f_obj, &f_coef),
	      start,
	      nb_history_points,
	      cv_tol,
	      id);

  var::dim = agent.dim();

  // create network server and client
  Network* network = new Network(agents_addresses, port);

  // initialize history files if relevant
  if(!save_to.empty() && !append)
    history::initialize_files(&agent, std::make_pair(&f_obj, &f_coef));


  if(global_loss != 0)
    agent.global_loss(global_loss);


  // run algorithm
  DecentralizedGradientDescent dgd(nb_local_iter,
				   nb_iter,
				   learning_rate,
				   batch);

  if(collab.isNotNull()) {
    arma::colvec collab_graph = Rcpp::as<arma::colvec>(collab);
    dgd.run_on_network(&agent, port, network, collab_graph);
  }
  else {
    dgd.run_on_network(&agent, port, network);
  }

  ConfInt confint(0.95);
  confint.compute_on_network(&agent, network);
  agent.confint(confint);

  f_coef.close();
  f_obj.close();

  delete network;


  // return agents formatted to R list
  return agent.to_list();
}
