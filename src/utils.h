#pragma once

#include <RcppArmadillo.h>
// [[Rcpp::depends(RcppArmadillo)]]

#include <vector>

/**
 * Generate a linear series of integers stored in a vector.
 *
 * The generated vector starts with `begin` and ends with `end`, with remaining
 * points generated uniformly between these two.
 *
 * @param begin first integer of the series.
 * @param end last integer of the series.
 * @param nb_points total number of points in the series.
 * If `nb_points > end - begin + 1`, all integers are present (only once), thus
 * it can generate series of lengths less than `nb_points`.
 *
 * @return a vector of integers with constant intervals between each element.
 */
std::vector<int> linspace(int begin,
			  int end,
			  int nb_points);

/**
 * Generate a vector of integers corresponding to agents to be activated for simulating an algorithm.
 *
 * @param nb_agents total number of agents.
 * @param nb_iter total number of iterations to generate activations for. One iteration
 * is thought to be of length `nb_agents`.
 * @param random_activations if set to `true`, generation will be generated uniformly
 * randomly. Otherwise it will be generated in a cyclic manner.
 *
 * @return a vector of length `nb_agents * nb_iter` of agent ids.
 */
std::vector< int > generate_activations(const int& nb_agents,
					const int& nb_iter,
					const bool& random_activations = true);

/**
 * Extracts the design matrix from a dataframe.*
 *
 * @param df the dataframe from which to extract.
 * @param label the column to remove from dataset since it is labels and therefore not design matrix.
 * @param intercept if set to true, add a column of `1` to the output.
 *
 * @param the design matrix to use in optimization algorithm.
 */
arma::mat extract_X_from_dataframe(const Rcpp::DataFrame&,
				   const std::string&,
				   const bool& intercept);

/**
 * Extracts the names of variables of design matrix from a dataframe.
 *
 * Note that this is different from the names of variables of the dataframe since it
 * removes the labels and adds a column names "(Intercept)" if relevant.
 *
 * @param df the dataframe from which to extract names.
 * @param label the column to remove from dataset since it is labels and therefore not design matrix.
 * @param intercept if set to true, add a column of `1` to the output.
 *
 * @return the names of the variables from the design matrix.
 */
Rcpp::CharacterVector extract_names_from_dataframe(const Rcpp::DataFrame&,
						   const std::string&,
						   const bool& intercept);

/**
 * Get number of columns of design matrix from a dataframe.
 *
 * Note that this is not just the number of column of the dataframe minus one, since
 * there can be an intercept or not.
 *
 * @param df the dataframe from which to extract names.
 * @param label the column to remove from dataset since it is labels and therefore not design matrix.
 * @param intercept if set to true, add a column of `1` to the output.
 *
 * @return dimension of the problem.
 */
int dimension(const Rcpp::DataFrame& df, const bool& intercept);
