#include "algorithm.h"

const int& Algorithm::nb_iter() {
  return nb_iter_;
}

void Algorithm::nb_iter(const int& nb_iter) {
  nb_iter_ = nb_iter;
}


void Algorithm::run(Agent* agent) {

  // initialize algorithm
  initialize(agent);

  // store initial value
  agent->update_history();


  for(int round = 0; round < nb_iter_; round++) {
    // update agent's iteration count
    agent->tick();

    // execute a local step
    local_iter(agent, round);

    // store values
    agent->update_history();

  }
}

void DecentralizedAlgorithm::run_on_network(Agent* agent, const int& port,
					    Network* network) {

  // generate default collaboration graph
  arma::colvec collab(network->nb_agents(), arma::fill::ones);
  collab = collab / network->nb_agents();

  run_on_network(agent, port, network, collab);

}

void DecentralizedAlgorithm::run_on_network(Agent* agent, const int& port,
					    Network* network,
					    const arma::colvec& collab) {

  // initialize
  var::init(port - 5001, network->nb_agents(), agent->dim());
  var::serv_vec = arma::conv_to< std::vector<float> >::from(agent->coefficients());

  acout(1) << "[main " << var::id << "] beginning computation" << std::endl;
  acout(1) << "[main " << var::id << "] wait for all connections" << std::endl;

  acout(0) << "[main " << var::id << "] started server on port " << port << std::endl;

  // initialize all that is needed for the algorithm
  initialize(agent);

  // initialize agent
  agent->initialize(nb_iter_);

  acout(0) << "[main " << var::id << "] initial coefficients: " <<
    arma::conv_to< std::vector<float> >::from(agent->coefficients()) << std::endl;
  var::serv_vec = arma::conv_to< std::vector<float> >::from(agent->coefficients());

  acout(0) << "[main " << var::id << "] finished connecting to all" << std::endl;

  for(int round = 0; round < nb_iter_; round++) {
    acout(0) << "[main " << var::id << "] beginning round " << round << std::endl;

    // initiliaze round
    var::init_round(round);

    // update iteration count of agent
    agent->tick();

    // perform a local step
    local_iter(agent, round);

    acout(0) << "[main " << var::id << "] setting local vector to " << agent->coefficients() << std::endl;

    // store it as a std::vector<float> (from arma::colvec)
    {
      std::unique_lock<std::mutex> lock(mtx::server);

      var::serv_vec = arma::conv_to< std::vector<float> >::from(agent->coefficients());
      var::serv_vector_ready = true;
    }

    // request vectors from others
    network->request_vectors(round);


    while(var::nb_sent < network->nb_agents() || var::nb_received < network->nb_agents()) {
      // wait until everything is sent and received
    }

    acout(1) << "[main " << var::id << "] received and sent all : " << var::nb_sent << ", " << var::nb_received << std::endl;
    {
      std::unique_lock<std::mutex> lock(mtx::client);

      // compute aggregated coefficient and use it as coefficients for the agent
      aggregate_results(agent, var::client_vec, collab);

      acout(0) << "[main " << var::id << "] round " << round << " coefficients: " <<
	arma::conv_to< std::vector<float> >::from(agent->coefficients()) << std::endl;

      acout(0) << "[main " << var::id << "] round " << round << " objective: " << agent->objective() << std::endl;
    }


    agent->update_history();
  }
}



void DecentralizedAlgorithm::run_locally(std::vector< Agent* > agents) {

  // set random activation to true by default
  run_locally(agents, true);

}

void DecentralizedAlgorithm::run_locally(std::vector< Agent* > agents,
					 const bool& random_activations) {

  // generate a default collaboration graph
  arma::mat collab_graph;
  int nb_agents = agents.size();

  collab_graph = arma::mat(nb_agents, nb_agents, arma::fill::ones);
  collab_graph = collab_graph / nb_agents;

  // run algorithm with this graph
  run_locally(agents, random_activations, collab_graph);

}


void DecentralizedAlgorithm::run_locally(std::vector< Agent* > agents,
					 const bool& random_activations,
					 const arma::mat& collab_graph) {
  // define some useful variables
  const int nb_agents = agents.size();

  // memory for current round's vector coefficients
  std::vector< std::vector<float> > coefficients(nb_agents);

  // initialize all agents
  for(unsigned int i = 0; i < agents.size(); i++) {
    // initialize
    agents[i]->initialize(nb_iter_);

    // store initial value
    agents[i]->update_history();
  }

  // initialize algorithm
  initialize_locally(agents);

  // run algorithm
  for(int round = 0; round < nb_iter_; round++) {

    // update each agent
    for(int i = 0; i < nb_agents; i++) {
      // update iteration count
      agents[i]->tick();

      // execute a local step on current agent
      local_iter(agents[i], round);

      // store result
      coefficients[i] = arma::conv_to< std::vector<float> >::from(agents[i]->coefficients());
    }

    // aggregate updates
    //    if(simplify_) {
      // compute value for one coefficient
      //aggregate_results(agents[0], coefficients, collab_graph.row(0).t());

      // set this value for all agents
    //      for(int i = 0; i < nb_agents; i++) {
	// set others coefficients
    //	agents[i]->coefficients(agents[0]->coefficients());

	// update history
    //	agents[i]->update_history();
    //      }
    //    }
    //    else {
    for(int i = 0; i < nb_agents; i++) {
      // update value of current agent
      aggregate_results(agents[i], coefficients, collab_graph.row(i).t());

      // update history
      agents[i]->update_history();
    }
    //    }
  }
  /*
  for(unsigned int i = 0; i < agent_activations.size(); i++) {

    // get activated agent for this round
    int id = agent_activations[i];
    Agent* agent = agents[id];

    // execute a local step on this agent
    local_iter(agent, i / nb_agents);

    // update the value of this agent by aggregating with others
    aggregate_results(agent, agents, collab_graph.row(id).t());

    // store values
    agent->update_history();

  }
  */

}
