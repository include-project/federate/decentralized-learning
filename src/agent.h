#pragma once

#include <RcppArmadillo.h>
// [[Rcpp::depends(RcppArmadillo)]]

#include <string>
#include <chrono>
#include <random>

#include "loss.h"
#include "utils.h"
#include "history.h"
#include "confint.h"

/**
 * A class gathering all elements needed in an optimization algorithm.
 *
 * This class contains two types of attributes:
 * - fixed optimization parameters: fit intercept, what to store during optimization,
 *   threshold for convergence...
 * - fixed data-related elements: dataset, variable names, loss, learning rate...
 * - optimization variables: optimized variable, iteration count...
 * - history variables.
 *
 */
class Agent {
public:
  /**
   * Base constructor.
   */
  Agent();

  /**
   * Contructor.
   *
   * @param df a dataframe containing all the data.
   * @param label the column of `df` to use as labels.
   * @param loss_name name of the loss to use.
   * @param intercept whether or not to fit the intercept.
   * @param coefficients default value for optmization coefficients.
   * @param nb_iter total number of iterations.
   * @param store_hstory if set to true, store all iterates, otherwise return only last iterate.
   * @param save_to file prefix for saving results.
   * @param start time at which we consider the algorithm starts running.
   * @param nb_history_point number of points to store in history variables.
   * @param cv_tol threshold of convergence.
   * @param id agent id. Used for storing history. (cf history object.)
   *
   */
  Agent(Rcpp::DataFrame df, std::string label,
	std::string loss_name, bool intercept,
	arma::colvec coefficients,
	int nb_iter, bool store_history,
	std::string save_to,
	std::chrono::steady_clock::time_point start,
	int nb_history_points,
	float cv_tol,
	int id = -1);

    /**
   * Contructor.
   *
   * @param df a dataframe containing all the data.
   * @param label the column of `df` to use as labels.
   * @param loss_name name of the loss to use.
   * @param intercept whether or not to fit the intercept.
   * @param coefficients default value for optmization coefficients.
   * @param nb_iter total number of iterations.
   * @param store_hstory if set to true, store all iterates, otherwise return only last iterate.
   * @param files file handles to use if provided.
   * @param start time at which we consider the algorithm starts running.
   * @param nb_history_point number of points to store in history variables.
   * @param cv_tol threshold of convergence.
   * @param id agent id. Used for storing history. (cf history object.)
   *
   */
  Agent(Rcpp::DataFrame df, std::string label,
	std::string loss_name, bool intercept,
	arma::colvec coefficients,
	int nb_iter, bool store_history,
	std::pair< std::ofstream*, std::ofstream* > files,
	std::chrono::steady_clock::time_point start,
	int nb_history_points,
	float cv_tol,
	int id = -1);



  /**
   * Contructor.
   *
   * @param X design matrix.
   * @param y labels.
   * @param label the column of `df` to use as labels.
   * @param loss_name name of the loss to use.
   * @param coefficients default value for optmization coefficients.
   * @param nb_iter total number of iterations.
   * @param store_hstory if set to true, store all iterates, otherwise return only last iterate.
   * @param save_to file prefix for saving results.
   * @param start time at which we consider the algorithm starts running.
   * @param nb_history_point number of points to store in history variables.
   * @param cv_tol threshold of convergence.
   * @param id agent id. Used for storing history. (cf history object.)
   *
   */
  Agent(arma::mat X,
	arma::vec y,
	std::string loss_name,
	arma::colvec coefficients,
	int nb_iter,
	bool store_history,
	std::string save_to,
	std::chrono::steady_clock::time_point start,
	int nb_history_points,
	Rcpp::CharacterVector names,
	float cv_tol,
	int id = -1);

  /**
   * Contructor.
   *
   * @param X design matrix.
   * @param y labels.
   * @param label the column of `df` to use as labels.
   * @param loss_name name of the loss to use.
   * @param coefficients default value for optmization coefficients.
   * @param nb_iter total number of iterations.
   * @param store_hstory if set to true, store all iterates, otherwise return only last iterate.
   * @param files file handles to use if provided.
   * @param start time at which we consider the algorithm starts running.
   * @param nb_history_point number of points to store in history variables.
   * @param cv_tol threshold of convergence.
   * @param id agent id. Used for storing history. (cf history object.)
   *
   */
  Agent(arma::mat X,
	arma::vec y,
	std::string loss_name,
	arma::colvec coefficients,
	int nb_iter,
	bool store_history,
	std::pair< std::ofstream*, std::ofstream* > files,
	std::chrono::steady_clock::time_point start,
	int nb_history_points,
	Rcpp::CharacterVector names,
	float cv_tol,
	int id = -1);



  /**
   * Destructor.
   */
  ~Agent();



  /**
   * Compute the objective.
   *
   * @return the value of the objective at current coefficients `coefficients_`.
   */
  float objective();


  /**
   * Compute the objective over a global dataset.
   *
   * @return if `global_loss_` is set, returns its objective at current coefficients `coefficients_`.
   * Otherwise return `-1`.
   */
  float global_objective();


  /**
   * Compute the gradient.
   *
   * @return the gradient at current coefficients `coefficients_`.
   */
  arma::colvec gradient(const bool& store = false);


  /**
   * Compute the Lipschitz constant of the objective.
   *
   * @return the Lipschitz constant.
   */
  float lipschitz();


  /**
   * Translates the `Agent` object to a `R` list.
   *
   * @return a `Rcpp::List` with all relevant attributes of the `Agent`.
   */
  Rcpp::List to_list();


  /**
   * Initialize `Agent` variables to run a new algorithm on it.
   *
   * @param nb_iter number of iterations to run algorithms for.
   */
  void initialize(const int& nb_iter);


  /**
   * Initialize `Agent` variables to run a new algorithm on it.
   *
   * @param nb_iter number of iterations to run algorithms for.
   * @param files file handles to use if provided.
   */
  void initialize(const int& nb_iter,
		  std::pair< std::ofstream*, std::ofstream* > files);


  /**
   * Initialize history variables.
   *
   * @param nb_iter number of iterations to run algorithms for.
   */
  void initialize_history(const int& nb_iter);


  /**
   * Store current values as the next point of history (if relevant).
   */
  void update_history();


  /**
   * Set global loss.
   *
   * @param global a pointer to a Loss object to use as loss.
   */
  void global_loss(Loss*);


  /**
   * Return current value of `coefficients_`.
   */
  arma::colvec coefficients();


  /**
   * Set value of `coefficients_`.
   *
   * @param coefficients New value of coefficients.
   */
  void coefficients(arma::colvec);


  /**
   * Return the dimension of the optimization problem carried by the `Agent`.
   */
  int dim();


  /**
   * Set the learning rate.
   *
   * @param learning_rate new value of learning rate.
   */
  void learning_rate(float lr);


  /**
   * Return the learning rate.
   */
  float learning_rate();

  /**
   * Return agent id.
   */
  int id();

  /**
   * Return names of variables the agent has.
   */
  Rcpp::CharacterVector names();
  
  /**
   * Return current iteration.
   */
  int iter();

  /**
   * Update iteration count.
   */
  void tick();

  /**
   * Return the file handles from the history object.
   */
  std::pair< std::ofstream*, std::ofstream* > file_handles();

  /**
   * Return last computed gradient.
   */
  arma::colvec last_gradient();

  /**
   * Return last update (value of delta such that x_t+1 = x_t + delta).
   */ 
  arma::colvec last_update();

  /**
   * Return design matrix.
   */
  arma::mat X();

  /**
   * Set confidence interval.
   */
  void confint(const ConfInt& ci);

private:
  /// Design matrix.
  arma::mat X_;

  /// Labels.
  arma::vec y_;

  /// Names of vaiables.
  Rcpp::CharacterVector names_;

  /// True of intercept should be fitted, false otherwise.
  bool intercept_;

  /// True if history should be stored, false otherwise.
  bool store_history_;

  /// Number of points to store in history.
  int nb_history_points_;

  /// Path to save to.
  std::string save_path_;

  /// Tolerance for convergence (if two iterates are closer than `cv_tol_`, the algorithm is considered to have converged).
  float cv_tol_;

  /// Training loss.
  Loss* loss_;

  /// Global ("testing") loss.
  Loss* global_loss_;

  /// Learning rate.
  float learning_rate_;


  /// Variable that is optimized.
  arma::colvec coefficients_;

  /// Variable that stores coefficients from last iterate.
  arma::colvec prev_coefficients_;

  /// Last computed gradient.
  arma::colvec last_gradient_;

  /// Last update.
  arma::colvec last_update_;
  
  /// Current iteration.
  int iter_;

  /// True of converged, false otherwise.
  bool converged_;

  /// History variables
  history history_;

  /// Agent id
  int id_;

  /// Confidence interval
  ConfInt confint_;
};
