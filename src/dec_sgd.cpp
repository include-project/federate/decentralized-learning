#include <RcppArmadillo.h>
// [[Rcpp::depends(RcppArmadillo)]]

#include <chrono>

#include "utils.h"
#include "agent.h"
#include "loss.h"


// [[Rcpp::export]]
Rcpp::List decentralized_sgd(std::vector< Rcpp::DataFrame > dfs,
			     Rcpp::Nullable< Rcpp::DataFrame > whole_df = R_NilValue,
			     Rcpp::Nullable< Rcpp::NumericMatrix > W = R_NilValue,
			     Rcpp::Nullable< Rcpp::NumericVector > w0 = R_NilValue,
			     std::string label = "y",
			     std::string loss_name = "least_squares",
			     bool intercept = true,
			     int batch = -1,
			     float momentum = 0,
			     float alpha = -1,
			     int nb_iter = 10,
			     float cv_tol = 1e-6,
			     bool random_activations = true,
			     int nb_history_points = 1000,
			     bool store_coef = true,
			     bool store_obj = true,
			     bool store_only_last = false) {

  // remember starting time
  std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();

  // number of agents
  int nb_agents = dfs.size();

  // number of dimensions
  int p = intercept ? dfs[0].length() : dfs[0].length() - 1;

  // generate agent activations
  std::vector<int> agent_activations = generate_activations(nb_agents, nb_iter, random_activations=random_activations);

  // if w0 not provided, fill it with zeros
  arma::colvec coefficients;
  if(w0.isNull())
    coefficients = arma::colvec(p, arma::fill::zeros);
  else
    coefficients = Rcpp::as<arma::colvec>(w0);


  // if a complete dataset is provided, compute global loss
  Loss* global_loss = 0;
  if(whole_df.isNotNull()) {
    Rcpp::DataFrame wdf = Rcpp::as<Rcpp::DataFrame>(whole_df);
    arma::mat globalX = extract_X_from_dataframe(wdf, label, intercept);
    arma::colvec globaly = wdf[label];

    global_loss = create_loss(globalX, globaly, loss_name);
  }

  // generate each agents parameters
  std::vector< Agent* > agents(nb_agents);

  for(int i = 0; i < nb_agents; i++) {
    // compute design matrix and labels from dataframe
    arma::mat X = extract_X_from_dataframe(dfs[i], label, intercept);
    arma::colvec y = dfs[i][label];

    // compute number of iter for current agent
    int nb_iter_curr = std::count(agent_activations.begin(), agent_activations.end(), i);

    // initialize agent
    agents[i] = new Agent(X, y, loss_name, coefficients, nb_iter_curr, store_only_last, "", start, nb_history_points, extract_names_from_dataframe(dfs[i], label, intercept), cv_tol);
    agents[i]->global_loss(global_loss);
  }


  // generate collaboration graph
  arma::mat collab_graph;
  if(W.isNull()) {
    collab_graph = arma::mat(nb_agents, nb_agents, arma::fill::ones);
    collab_graph = collab_graph / nb_agents;
  }
  else
    collab_graph = Rcpp::as< arma::mat >(W);


  // compute learning rate if not provided
  if(alpha < 0) {
    float min_lip = -1;
    for(int i = 0; i < nb_agents; i++) {
      if(min_lip == -1 || agents[i]->lipschitz() < min_lip)
	min_lip = agents[i]->lipschitz();
    }
    alpha = 1.0 / (2 * min_lip + sqrt(nb_agents / p));
  }

  // run the algorithm !

  for(int i = 0; i < nb_agents * nb_iter; i++) {
    // check for keyboard interrupt
    R_CheckUserInterrupt();

    // get current agent
    Agent* curr_agent = agents[agent_activations[i]];

    // compute local gradient
    arma::colvec grad = curr_agent->gradient();

    // compute average model from neighbors
    arma::colvec avg_coef(p, arma::fill::zeros);
    for(int neighbor = 0; neighbor < nb_agents; neighbor++) {
      avg_coef += collab_graph(agent_activations[i], neighbor) * (agents[neighbor])->coefficients();
    }

    curr_agent->coefficients(avg_coef - alpha * grad);
    curr_agent->update_history();
  }

  Rcpp::List ret = Rcpp::List::create();
  for(int i = 0; i < nb_agents; i++)
    ret.push_back(agents[i]->to_list());

  return ret;
}
