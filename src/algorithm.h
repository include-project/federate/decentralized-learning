#pragma once

#define RCPPTHREAD_OVERRIDE_COUT 1
#define RCPPTHREAD_OVERRIDE_THREAD 1

// [[Rcpp::depends(BH)]]


#include <thread>
#include <RcppArmadillo.h>
// [[Rcpp::depends(RcppArmadillo)]]

#include <vector>

#include "network.h"
#include "network_address.h"
#include "agent.h"
#include "acout.h"
#include "env.h"
#include "learning_rate.h"


/*
 * Abstract class for algorithms.
 *
 * This class is the general framework for defining iterative algorithm.
 * The functions `initialize` and `local_iter` should be redefined by derived classes.
 * The `run` function then call all functions appropriately.
 */
class Algorithm {
public:
  /**
   * Initialize the algorithm.
   *
   * @param Agent* a pointer to an agent to initialize with algorithm-dependent elements.
   */
  virtual void initialize(Agent* agent) = 0;

  /**
   * Local iteration.
   *
   * This function is the function that is run at each iteration of the algorithm.
   * In the case of Gradient Descent, it simply does one gradient step.
   */
  virtual void local_iter(Agent* agent, const int& round) = 0;

  /**
   * Return total number of iterations.
   */
  const int& nb_iter();

  /**
   * Set the total number of iterations.
   *
   * @param nb_iter total number of iterations.
   */
  void nb_iter(const int& nb_iter);

  /**
   * Run the algorithm on an agent.
   *
   * @param agent a pointer to the agent on which to run the algorithm.
   */
  void run(Agent* agent);

protected:
  /// Learning rate.
  LearningRate* learning_rate_;

  /// Total number of iterations.
  int nb_iter_;
};


/*
 * Abstract class for algorithms.
 *
 * This class is the general framework for defining iterative algorithm.
 * The functions `initialize` and `local_iter` should be redefined by derived classes.
 * The `run` function then call all functions appropriately.
 */
class DecentralizedAlgorithm : public Algorithm {
public:
  /**
   * Initialize the algorithm.
   *
   * @param Agent* a pointer to an agent to initialize with algorithm-dependent elements.
   */
  virtual void initialize(Agent* agent) = 0;


  /**
   * Initialize the algorithm for a simulated run.
   *
   * @param std::vetor< Agent* > a vector of pointers to agents to initialize with algorithm-dependent elements.
   */
  virtual void initialize_locally(std::vector< Agent* > agents) = 0;


  /**
   * Scheme to follow for aggregating result.
   *
   * @param agent the `Agent` being updated.
   * @param other_coefficients coefficients from other agents part-taking in the optimization.
   * @param collab collaboration matrix corresponding to the optimized `Agent`.
   */
  virtual void aggregate_results(Agent* agent, const std::vector< std::vector<float> >& other_coefficients,
				 const arma::colvec& collab) = 0;

  /**
   * Scheme to follow for aggregating result.
   *
   * @param agent the `Agent` being updated.
   * @param other_agents other `Agent`s part-taking in the optimization.
   * @param collab collaboration matrix corresponding to the optimized `Agent`.
   */
  virtual void aggregate_results(Agent* agent, std::vector< Agent* > other_agents,
				 const arma::colvec& collab) = 0;


  /**
   * Simulate the algorithm locally.
   *
   * This function automatically builds a collaboration graph where all agents have uniform dependence on each other.
   * Agents are also activated randomly if this function is called.
   *
   * @param agents a list of agents on which to run the algorithm on.
   */
  void run_locally(std::vector< Agent* > agents);


  /**
   * Simulate the algorithm locally.
   *
   * This function automatically builds a collaboration graph where all agents have uniform dependence on each other.
   *
   * @param agents a list of agents on which to run the algorithm on.
   * @param random_activations whether activations of `Agent`s should be random or cyclic.
   */
  void run_locally(std::vector< Agent* > agents, const bool& random_activations);


  /**
   * Simulate the algorithm locally.
   *
   *
   * @param agents a list of agents on which to run the algorithm on.
   * @param random_activations whether activations of `Agent`s should be random or cyclic.
   * @param collab_graph collaboration graph, should be doubly stochastic (sum of row and column is 1).
   */
  void run_locally(std::vector< Agent* > agents, const bool& random_activations,
		   const arma::mat& collab_graph);


  /**
   * Run the algorithm on the network.
   *
   * The same function should be running at other agents with addresses in `distant_agents`.
   * By default, the collaboration row corresponding to local agent is set to a vector of the form `(1/p,...,1/p)`, where `p` is the dimension.
   *
   * @param agent local agent to optimize.
   * @param port port on which to start the server on.
   * @param network network.
   */
  void run_on_network(Agent* agent, const int& port, Network* network);


  /**
   * Run the algorithm on the network.
   *
   * @param agent local agent to optimize.
   * @param port port on which to start the server on.
   * @param network network.
   * @param collab collaboration graph's row corresponding to local agent (should sum to 1).
   */
  void run_on_network(Agent* agent, const int& port, Network* network,
		      const arma::colvec& collab);



protected:
  /// number of iterations the Agent is going to do during optimization.
  int nb_local_iter_;

  /// If simplify_ is set to true, the aggregation will be done only once
  /// then set as value of all agents.
  bool simplify_;
};


/**
 * Gradient Descent algorithm.
 */
class GradientDescent : public Algorithm {
public:
  /**
   * Constructor.
   *
   * @param nb_iter number of iterations to run the algorithms for.
   * @param learning_rate learning rate.
   * @param batch number of data points to use for each iteration.
   * @param momentum value of the momentum in the algorithm.
   */
  GradientDescent(const int& nb_iter,
		  LearningRate* learning_rate = 0,
		  const int& batch = -1, const float& momentum = 0);

  /**
   * Destructor.
   */
  ~GradientDescent();

  /**
   * Initialize agent to run the algorithm on.
   *
   * @param agent a pointer to the agent to initialize.
   */
  void initialize(Agent* agent);

  /**
   * The set of computation to do at each iteration.
   *
   * @param agent the agent to run the iterations on.
   * @param round number of the current iteration.
   */
  void local_iter(Agent* agent, const int& round);

private:
  /// Size of batches.
  int batch_;

  /// Value of momentum.
  float momentum_;
};


/**
 * Decentralized Gradient Descent algorithm.
 */
class DecentralizedGradientDescent : public DecentralizedAlgorithm {
public:
  /**
   * Constructor.
   *
   * @param nb_local_iter number of iterations to perform locally before communicating the results.
   * @param nb_iter number of round of communications.
   * @param learning_rate learning rate.
   * @param batch number of data points to use for each iteration.
   * @param momentum value of momentum.
   */
  DecentralizedGradientDescent(const int& nb_local_iter, const int& nb_iter,
			       LearningRate* learning_rate,
			       const int& batch = -1,
			       const bool& simplify = false);

  /**
   * Destructor.
   */
  ~DecentralizedGradientDescent();


  /**
   * Initialize agent to run the algorithm on.
   *
   * @param agent a pointer to the agent to initialize.
   */
  void initialize(Agent* agent);

  /**
   * Initialize the set of agents to run the algorithm on.
   *
   * @param agents a vector of pointer to `Agent`s to initialize.
   */
  void initialize_locally(std::vector< Agent* > agents);

  /**
   * Instructions to perform for each local iteration.
   *
   * @param agent the agent on which to perform the local iteration.
   * @param round number of the current round.
   */
  void local_iter(Agent* agent, const int& round);

  /**
   * Function to aggregate results from other `Agent`'s results.
   *
   * This function is thought to be executed when the algorithm is run over the network.
   *
   * @param agent the agent from whom values will be updated.
   * @param other_coefficients a vector with the coefficients from other agents.
   * @param collab the row of the collaboration graph corresponding to the local `Agent`.
   */
  void aggregate_results(Agent* agent, const std::vector< std::vector<float> >& other_coefficients,
			 const arma::colvec& collab);

  /**
   * Function to aggregate results from other `Agent`'s results.
   *
   * This function is thought to be executed when the algorithm is run locally.
   *
   * @param agent the agent from whom values will be updated.
   * @param other_agents a vector with pointers to other `Agent`s taking part in the computation..
   * @param collab the row of the collaboration graph corresponding to the local `Agent`.
   */
  void aggregate_results(Agent* agent, std::vector< Agent* > other_agents, const arma::colvec& collab);

private:
  /// Batch size.
  int batch_;

  /// Decay.
  float decay_;

  /// Accumulated gradient.
  arma::vec acc_grad_;

  /// Accumulated updates.
  arma::vec acc_update_;
};
