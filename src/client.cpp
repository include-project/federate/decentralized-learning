#include "client.h"

const std::vector<float> client_session::heartbeat_message_ = std::vector<float>(2, message_code::heartbeat);


// Begin Client Session functions

client_session::pointer client_session::create(asio::io_service& io_service, tcp_client* client, const int& server_id) {
  return pointer(new client_session(io_service, client, server_id));
}

client_session::client_session(asio::io_service& io_service, tcp_client* client, const int& server_id)
  : socket_(io_service),
    server_id_(server_id),
    buffer_(var::dim + 1),
    heartbeat_timer_(io_service, std::chrono::seconds(50)),
    stopped_(false),
    client_pointer_(client) {
  acout(2) << "[client " << var::id << " -> " << server_id_ << "] created" << std::endl;
}

client_session::~client_session() {
  acout(1) << "[client " << var::id << " -> " << server_id_ << "] terminated" << std::endl;
  client_pointer_->delete_session();

}


// getter
tcp::socket& client_session::socket() {
  return socket_;
}


void client_session::start() {
  acout(2) << "[client " << var::id << " -> " << server_id_ << "] start()" << std::endl;
  heartbeat_timer_.async_wait(boost::bind(&client_session::do_heartbeat, shared_from_this()));
  do_read_start();
}


// states of the session
void client_session::do_read_start() {
  acout(2) << "[client " << var::id << " -> " << server_id_ << "] do_read_start()" << std::endl;

  // reset heartbeat timer (no need for heartbeat if other messages are pending)
  heartbeat_timer_.expires_from_now(std::chrono::seconds(HEARTBEAT_DELAY));

  asio::async_read(socket_, asio::buffer(buffer_),
			  asio::transfer_at_least(1),
			  boost::bind(&client_session::handle_read, shared_from_this(),
				      asio::placeholders::error,
				      asio::placeholders::bytes_transferred)
			  );
}

// states of the session
void client_session::do_read() {
  acout(2) << "[client " << var::id << " -> " << server_id_ << "] do_read()" << std::endl;

  // reset heartbeat timer (no need for heartbeat if other messages are pending)
  heartbeat_timer_.expires_from_now(std::chrono::seconds(HEARTBEAT_DELAY));

  asio::async_read(socket_, asio::buffer(buffer_),
			  asio::transfer_at_least(1),
			  boost::bind(&client_session::handle_read, shared_from_this(),
				      asio::placeholders::error,
				      asio::placeholders::bytes_transferred)
			  );
}


void client_session::handle_read(const asio::error_code& error, size_t number_bytes_read) {
  acout(2) << "[client " << var::id << " -> " << server_id_ << "] handle_read(), read " << number_bytes_read << " bytes" << std::endl;

  if(!error) {
    // compute code from first element of buffer_
    // it is sent as float so we make sure to avoid rounding errors
    int code = floor(buffer_[0] + 0.5);
    
    if(code == message_code::welcome) {
      // at this time only are we sure that we can send queries to server
      client_pointer_->add_session();
      //network::nb_connected_to++;


      acout(1) << "[client " << var::id << " -> " << server_id_ << "] received welcome message from server " << server_id_ << std::endl;
    }
    
    else if(code == message_code::vector || code == message_code::cov_matrix) {
      // elements after the first one are the vector we want
      std::vector<float> received_vec(buffer_.begin() + 1, buffer_.end());
      
      // store vector (which sets the promise that this connection received the vector)
      var::client_vec[server_id_] = received_vec;

      // update number of received
      var::nb_received++;
      received_ = true;
      
      acout(1) << "[client " << var::id << " -> " << server_id_ << "] received vector from " << server_id_ << ": " << received_vec << " (" << var::nb_received << " received)." << std::endl;
    }
    
    else {
      acout(1) << "[client " << var::id << " -> " << server_id_ << "] received unknown message from " << server_id_ << ": " << buffer_ << std::endl;
    }
  }
  
  else if (error == asio::error::eof) {
    acout(1) << "[client " << var::id << " -> " << server_id_ << "] connection killed by server" << std::endl;
  }
  
  else {
    acout(1) << "[client " << var::id << " -> " << server_id_ << "] received error: " << error.message() << std::endl;
  }
  
  
  
}

void client_session::request_vector_from_server(const int& round, const int& message) {
  acout(2) << "[client " << var::id << " -> " << server_id_ << "] do_write()" << std::endl;

  // wait for current round computation to finish
  // var::current_round_finished.get_future().get();

  // build message to ask for other vectors
  if(message == message_code::ask_vector) {
    message_ = std::vector<float>(2);
    message_[0] = message_code::ask_vector;
    message_[1] = round;

    // expect a message of length dim+1
    buffer_.resize(var::dim + 1);
  }
  else if(message == message_code::ask_cov_matrix) {
    message_ = std::vector<float>(2);
    message_[0] = message_code::ask_cov_matrix;
    message_[1] = round;

    // expect a message of length dim**2+1
    buffer_.resize(var::dim * var::dim + 1);
  }

  received_ = false;

  // reset heartbeat timer (no need for heartbeat if other messages are pending)
  heartbeat_timer_.expires_from_now(std::chrono::seconds(HEARTBEAT_DELAY));

  
  // send it to server
  asio::async_write(socket_, asio::buffer(message_),
			   boost::bind(&client_session::handle_write, shared_from_this(),
				       asio::placeholders::error,
				       asio::placeholders::bytes_transferred));  
}

void client_session::terminate() {
  // cancel the timer as it holds a pointer to the session,
  // therefore removing it will terminate gracefully the session
  stopped_ = true;
  heartbeat_timer_.cancel();
}

void client_session::handle_write(const asio::error_code& error, size_t number_bytes_transferred) {
  acout(2) << "[client " << var::id << " -> " << server_id_ << "] handle_write() with error " << error.message() << std::endl;

  if (!error) {
    do_read();
  }
  else {
    //    std::cerr << error.message();
  }
}

void client_session::do_heartbeat() {
  if(heartbeat_timer_.expiry() <= std::chrono::steady_clock::now()) {
    acout(2) << "[client " << var::id << " -> " << server_id_ << "] do_heartbeat()" << std::endl;

    // Start an asynchronous operation to send a heartbeat message.
    asio::async_write(socket_, asio::buffer(heartbeat_message_),
		      boost::bind(&client_session::handle_heartbeat, shared_from_this(),
				  asio::placeholders::error,
				  asio::placeholders::bytes_transferred));
  }
  else {
    heartbeat_timer_.expires_from_now(std::chrono::seconds(HEARTBEAT_DELAY));
    heartbeat_timer_.async_wait(boost::bind(&client_session::do_heartbeat, shared_from_this()));
  }
}

void client_session::handle_heartbeat(const asio::error_code& error, size_t number_bytes_transferred) {
  acout(2) << "[client " << var::id << " -> " << server_id_ << "] handle_heartbeat()" << std::endl;

  if (!error && !stopped_) {
      // Wait HEARTBEAT_DELAY seconds before sending the next heartbeat.
      heartbeat_timer_.expires_from_now(std::chrono::seconds(HEARTBEAT_DELAY));
      heartbeat_timer_.async_wait(boost::bind(&client_session::do_heartbeat, shared_from_this()));
  }
  else {
    //    std::cerr << error.message();
  }
}


// End Client Session


// TCP Client
tcp_client::tcp_client(asio::io_service& io_service)
  : io_service_(io_service),
    connections_(),
    nb_active_session_(0),
    ready_(false) {

}

tcp_client::~tcp_client() {

}

void tcp_client::request_vectors(const int& round, const int& message) {
  for(auto& elt : connections_) {
    elt.second->request_vector_from_server(round, message);
  }
}

void tcp_client::connect(tcp::endpoint& endpoint, const int& server_id) {
  // create a connection to a server
  acout(2) << "[bigclient " << var::id << "] trying to connect to server " << server_id << std::endl;

  // create a new session
  client_session::pointer new_session = client_session::create(io_service_, this, server_id);

  // create a timer for this server in case reconnection is needed
  connection_timers_[server_id] =
    boost::shared_ptr<asio::steady_timer>(
      new asio::steady_timer(io_service_,
				      std::chrono::seconds(1000)));

  // try to connect
  tcp::socket& socket = new_session->socket();

  socket.async_connect(endpoint,
		       boost::bind(&tcp_client::handle_connect, this,
				   new_session,
				   endpoint,
				   server_id,
				   asio::placeholders::error)
		       );
}

void tcp_client::reconnect(tcp::endpoint& endpoint, const int& server_id, client_session::pointer new_session) {
  // create a connection to a server
  acout(2) << "[bigclient " << var::id << "] trying to reconnect to server " << server_id << std::endl;


  // create a timer for this server in case reconnection is needed
  connection_timers_[server_id] =
    boost::shared_ptr<asio::steady_timer>(
      new asio::steady_timer(io_service_,
				      std::chrono::seconds(1000)));

  // try to connect
  tcp::socket& socket = new_session->socket();

  socket.async_connect(endpoint,
		       boost::bind(&tcp_client::handle_connect, this,
				   new_session,
				   endpoint,
				   server_id,
				   asio::placeholders::error)
		       );
}

void tcp_client::handle_connect(client_session::pointer new_session, tcp::endpoint& endpoint, const int& server_id, const asio::error_code& error) {
  // if connection succeeded
  if(!error) {
    acout(2) << "[bigclient " << var::id << "] connection to " << server_id << " succeeded with code : " << error.message() << std::endl;

    // timer for reconnection is no longer useful
    connection_timers_.erase(server_id);

    // keep a pointer to this session
    connections_[server_id] = new_session;
    
    // new session starts with reading
    new_session->start();
  }

  // if connection failed
  else {
    acout(2) << "[bigclient " << var::id << "] connection to " << server_id << " failed" << std::endl;

    // try to reconnect in some time
    connection_timers_[server_id]->expires_from_now(std::chrono::milliseconds(1000));
    connection_timers_[server_id]->async_wait(std::bind(&tcp_client::on_ready_to_reconnect,
							this,
							endpoint,
							server_id,
							new_session));

  }
}

void tcp_client::on_ready_to_reconnect(tcp::endpoint& endpoint, const int& server_id, client_session::pointer new_session) {
  acout(2) << "[bigclient " << var::id << "] try to reconnect to " << server_id << std::endl;

  reconnect(endpoint, server_id, new_session);
}

void tcp_client::terminate() {
  acout(2) << "[bigclient " << var::id << "] terminating client" << std::endl;


  auto it_session = connections_.begin();
  while(it_session != connections_.end()) {
    // terminate connection
    it_session->second->terminate();

    // delete it and get next iterator
    it_session = connections_.erase(it_session);
  }
  
  //  for(unsigned int i = 0; i < connections_.size(); i++) {
  //  }

  auto it_timer = connection_timers_.begin();
  while(it_timer != connection_timers_.end()) {
    // delete it and get next iterator
    it_timer = connection_timers_.erase(it_timer);
  }
  
  io_service_.stop();
}

int tcp_client::nb_active_session() {
  return nb_active_session_;
}

void tcp_client::add_session() {
  nb_active_session_++;
}

void tcp_client::delete_session() {
  nb_active_session_--;
}

bool tcp_client::ready() {
  return ready_;
}

void tcp_client::ready(const bool& r) {
  ready_ = r;
}
