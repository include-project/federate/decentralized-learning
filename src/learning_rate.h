#pragma once

#include <iostream>
#include <string>
#include <vector>
#include <RcppArmadillo.h>

#include "agent.h"

/*
 * Abstract class for learning rates.
 *
 * This class is the general framework for defining learning rates.
 * The functions `get` and `update` should be defined in all derived classes.
 */
class LearningRate {
 public:
  /**
   * Return the value of the current learning rate.
   */
  virtual arma::colvec get() = 0;

  /**
   * Updates the learning rate with current optimization variables.
   *
   * @param agent agent from to take variables.
   * @param round current round of the algorithm.
   */ 
  virtual void update(Agent* agent, const int& round) = 0;

 protected:
  /// Current value of the learning rate.
  arma::colvec value_;

  /// Dimension of the problem.
  int dim_;
};



class ConstLearningRate : public LearningRate {
 public:
  /**
   * Constructor.
   *
   * @param learning_rate value of the constant learning rate. If set to a negative value,
   * an automatic learning rate will be used.
   * @param dim dimension of the problem.
   */
  ConstLearningRate(const float& learning_rate, const int& dim);

  /**
   * Return the value of the learning rate.
   */
  arma::colvec get();

  /**
   * Updates the learning rate with current optimization variables.
   *
   * @param agent agent from to take variables.
   * @param round current round of the algorithm.
   */ 
  void update(Agent* agent, const int& round);
};


class AdaDeltaLearningRate : public LearningRate {
 public:
  /** 
   * Constructor.
   *
   * @param decay decay parameter.
   * @param dim dimension of the problem.
   * @param eps a value close to zero to avoid numerical problems.
   */
  AdaDeltaLearningRate(const float& decay, const int& dim, const float& eps=1e-6);

  /**
   * Return the value of the learning rate.
   */
  arma::colvec get();
  
  /**
   * Updates the learning rate with current optimization variables.
   *
   * @param agent agent from to take variables.
   * @param round current round of the algorithm.
   */ 
  void update(Agent* agent, const int& round);

 private:
  /// Decay parameter.
  float decay_;

  /// A little value to provide numerical stability.
  float eps_;

  /// Accumulated values of gradients.
  arma::colvec acc_grad_;

  /// Accumulated values of updates.
  arma::colvec acc_update_;
};

/**
 * Creates a learning rate object.
 *
 * @param learning_rate_type a string describing the type of learning rate to use.
 * @param learning_rate_param parameters of the learning rate.
 * @param dim dimension of the problem.
 */
LearningRate* create_learning_rate(const std::string& learning_rate_type,
				   Rcpp::Nullable< std::vector<float> > learning_rate_param,
				   const int& dim);
