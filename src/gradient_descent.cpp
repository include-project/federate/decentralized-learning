#include "algorithm.h"


GradientDescent::GradientDescent(const int& nb_iter, LearningRate* learning_rate,
				 const int& batch, const float& momentum)
  : batch_(batch),
    momentum_(momentum) {
  nb_iter_ = nb_iter;

  learning_rate_ = learning_rate;
}

GradientDescent::~GradientDescent() {

}

void GradientDescent::initialize(Agent* agent) {

  // initialize agent
  agent->initialize(nb_iter_);

  // compute learning rate if not provided
  if(learning_rate_ == 0) 
    learning_rate_ = new ConstLearningRate(1.0 / agent->lipschitz(), agent->dim());

}

void GradientDescent::local_iter(Agent* agent, const int& round) {

  // compute gradient
  arma::colvec grad = agent->gradient(true);

  // update learning rate
  learning_rate_->update(agent, round);

  // update coefficients
  arma::colvec new_coefficients = agent->coefficients() - learning_rate_->get() % grad;

  agent->coefficients(new_coefficients);

}
