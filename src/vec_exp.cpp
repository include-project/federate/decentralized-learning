#include "vec_exp.h"

arma::vec vecexp(arma::vec v) {

  arma::vec ret = arma::vec(v.n_elem);

  for(int i = 0; i < ret.n_elem; i++)
    ret[i] = exp(v[i]);

  return ret;
}

arma::vec veclog(arma::vec v) {

  arma::vec ret = arma::vec(v.n_elem);

  for(int i = 0; i < ret.n_elem; i++)
    ret[i] = log(v[i]);

  return ret;
}
