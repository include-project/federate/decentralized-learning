#include "history.h"
#include "agent.h"


history::history() {

}

history::history(Agent* agent,
		 const int& nb_iter,
		 const int& nb_stored_points,
		 const bool& store,
		 std::pair< std::ofstream*, std::ofstream* > files)
  : agent_(agent),
    store_(store),
    save_path_(""),
    start_(std::chrono::steady_clock::now()),
    last_stored_point_(0) {

  // store pointers to file handles
  f_obj_ = files.first;
  f_coef_ = files.second;

  save_to_file_ = true;

  x_ = linspace(0, nb_iter, nb_stored_points);

  initialize_all(agent, nb_iter, nb_stored_points, store, "", false);


}

history::history(Agent* agent,
		 const int& nb_iter,
		 const int& nb_stored_points,
		 const bool& store,
		 const std::string& save_path)
  : agent_(agent),
    store_(store),
    save_path_(save_path),
    start_(std::chrono::steady_clock::now()),
    last_stored_point_(0) {


  x_ = linspace(0, nb_iter, nb_stored_points);

  initialize_all(agent, nb_iter, nb_stored_points, store, save_path, true);
}

void history::store() {

  // if should store
  // and if it is time to store
  if((store_ || !save_path_.empty() || save_to_file_) &&
     x_[last_stored_point_] == agent_->iter()) {

    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();

    float obj = agent_->objective();
    float full_obj = agent_->global_objective();
    float time = std::chrono::duration_cast<std::chrono::microseconds> (end - start_).count() * 1e-6;
    arma::rowvec coefficients = trans(agent_->coefficients());

    if(store_) {
      store_in_memory(obj, full_obj, time, coefficients);
    }
    if(!save_path_.empty() || save_to_file_) {
      store_in_file(obj, full_obj, time, coefficients);
    }

    last_stored_point_++;
  }

}

Rcpp::Nullable< Rcpp::List > history::to_list() {
  // if stored in memory return a Rcpp::List
  if(store_) {
    return Rcpp::List::create(
      Rcpp::Named("x") = Rcpp::wrap(x_),
      Rcpp::Named("objective") = Rcpp::wrap(obj_),
      Rcpp::Named("full_objective") = Rcpp::wrap(full_obj_),
      Rcpp::Named("time") = Rcpp::wrap(time_),
      Rcpp::Named("coefficients") = Rcpp::wrap(coefficients_));
    }

  // if not stored in memory return NULL
  else {
    return R_NilValue;
  }
}

void history::initialize_files(Agent* agent, std::string save_to) {
  std::ofstream f_coef, f_obj;

  // open file for writing coefficients values and erase its content
  f_coef.open(save_to + "_coef.csv");

  // open file for writing objective values and erase its content
  f_obj.open(save_to + "_obj.csv");

  initialize_files(agent, std::make_pair(&f_coef, &f_obj));

  f_coef.close();
  f_obj.close();

}

void history::initialize_files(Agent* agent,
			       std::pair< std::ofstream*, std::ofstream* > files) {

  std::ofstream* f_obj = files.first;
  std::ofstream* f_coef = files.second;

  // if id is positive, each line might correspond to a different agent
  // therefore we save its id (and add a column for that)
  if(agent->id() >= 0) {
    *f_obj << "agent, ";
    *f_coef << "agent, ";
  }

  *f_coef << "iteration, ";

  // write agent's variable names as header
  for(int i = 0; i < agent->dim(); i++) {
    *f_coef << agent->names()[i];

    if(i < agent->dim() - 1)
      *f_coef << ", ";
  }
  *f_coef << std::endl;

  // write the rest of the header
  *f_obj << "iteration, time, objective, full objective" << std::endl;
}


std::pair< std::ofstream*, std::ofstream* > history::file_handles() {
  return std::make_pair(f_obj_, f_coef_);
}

void history::reinitialize(const int& nb_iter, const int& nb_stored_points) {
  x_ = linspace(0, nb_iter, nb_stored_points);
}


void history::initialize_all(Agent* agent, const int& nb_iter, const int& nb_stored_points,
			 const bool& store, const std::string& save_path, const bool& init_file_handles) {

  // compute iterations that should be stored

  // if memory has to be initialized, initialize it
  if(store) {
    obj_ = std::vector<float>(x_.size(), 0);
    full_obj_ = std::vector<float>(x_.size(), 0);
    rmse_ = std::vector<float>(x_.size(), 0);
    time_ = std::vector<float>(x_.size(), 0);
    coefficients_ = arma::mat(x_.size(), agent->dim());

    save_to_file_ = false;
  }

  // otherwise open relevant files
  if(!save_path_.empty() && init_file_handles) {
    // create the handles
    f_coef_ = new std::ofstream();
    f_obj_ = new std::ofstream();

    // open the files
    f_coef_->open(save_path_ + "_coef.csv", std::ios::app);
    f_obj_->open(save_path_ + "_obj.csv", std::ios::app);

    save_to_file_ = true;

  }
}



void history::store_in_memory(float obj, float full_obj, float time, arma::rowvec coefficients) {
  obj_[last_stored_point_] = obj;
  full_obj_[last_stored_point_] = full_obj;
  time_[last_stored_point_] = time;
  coefficients_.row(last_stored_point_) = coefficients;

}

void history::store_in_file(float obj, float full_obj, float time, arma::rowvec coefficients) {

  int i = x_[last_stored_point_];

  // if agent's id is filled (>=0), write it at beginning of line
  if(agent_->id() >= 0) {
    *f_obj_ << agent_->id() << ", ";
    *f_coef_ << agent_->id() << ", ";
  }
  // write rest of line
  *f_obj_ << i << ", "
	<< time << ", "
	<< obj << ", "
	<< full_obj << std::endl;

  // write coefficients
  *f_coef_ << i << ", ";
  for(unsigned int i = 0; i < coefficients.n_elem; i++) {
    *f_coef_ << coefficients[i];

    if(i < coefficients.n_elem - 1)
      *f_coef_ << ", ";
  }
  *f_coef_ << std::endl;

}
