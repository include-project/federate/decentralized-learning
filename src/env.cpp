#include "env.h"


/*
namespace network {
  tcp_server* server_pointer = 0;
  tcp_client* client_pointer = 0;

  bool server_ready = false;
  bool client_ready = false;
  int nb_connected_to = 0;
  int nb_server_session = 0;

}
*/

std::mutex mtx::cout;
std::mutex mtx::server;
std::mutex mtx::client;

int var::id, var::nb_agents, var::dim;
int var::verbose = 3;

int var::round;

int var::nb_sent;

int var::nb_received;

bool var::serv_vector_ready;

std::vector<float> var::serv_vec;
std::vector< std::vector<float> > var::client_vec;
