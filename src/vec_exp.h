#pragma once

#include <RcppArmadillo.h>

arma::vec vecexp(arma::vec v);
arma::vec veclog(arma::vec v);
