#include <RcppArmadillo.h>
// [[Rcpp::depends(RcppArmadillo)]]

#include <chrono>

#include "utils.h"
#include "agent.h"
#include "utils.h"
#include "loss.h"


// [[Rcpp::export]]
Rcpp::List decentralized_personalized_gd(std::vector< Rcpp::DataFrame > dfs,
					 Rcpp::Nullable< Rcpp::DataFrame > whole_df = R_NilValue,
					 Rcpp::Nullable< Rcpp::NumericMatrix > W = R_NilValue,
					 Rcpp::Nullable< Rcpp::NumericVector > w0 = R_NilValue,
					 std::string label = "y",
					 std::string loss_name = "least_squares",
					 float mu = 1,
					 bool intercept = true,
					 int batch = -1,
					 float momentum = 0,
					 int nb_iter = 10,
					 float cv_tol = 1e-6,
					 bool random_activations = true,
					 int nb_history_points = 1000,
					 bool store_coef = true,
					 bool store_obj = true,
					 bool store_only_last = false) {

  // remember starting time
  std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();

  // number of agents
  int nb_agents = dfs.size();

  // number of dimensions
  int p = intercept ? dfs[0].length() : dfs[0].length() - 1;

  // generate agent activations
  std::vector<int> agent_activations = generate_activations(nb_agents, nb_iter, random_activations=random_activations);

  // if w0 not provided, fill it with zeros
  arma::colvec coefficients;
  if(w0.isNull())
    coefficients = arma::colvec(p, arma::fill::zeros);
  else
    coefficients = Rcpp::as<arma::colvec>(w0);


  // if a complete dataset is provided, compute global loss
  Loss* global_loss = 0;
  if(whole_df.isNotNull()) {
    Rcpp::DataFrame wdf = Rcpp::as<Rcpp::DataFrame>(whole_df);
    arma::mat globalX = extract_X_from_dataframe(wdf, label, intercept);
    arma::colvec globaly = wdf[label];

    global_loss = create_loss(globalX, globaly, loss_name);
  }

  // generate each agents parameters
  std::vector< Agent* > agents(nb_agents);

  for(int i = 0; i < nb_agents; i++) {
    // compute design matrix and labels from dataframe
    arma::mat X = extract_X_from_dataframe(dfs[i], label, intercept);
    arma::colvec y = dfs[i][label];

    // compute number of iter for current agent
    int nb_iter_curr = std::count(agent_activations.begin(), agent_activations.end(), i);

    // initialize agent
    agents[i] = new Agent(X, y, loss_name, coefficients, nb_iter_curr, store_only_last, "", start, nb_history_points, extract_names_from_dataframe(dfs[i], label, intercept), cv_tol);
    agents[i]->global_loss(global_loss);
  }

  // generate collaboration graph
  arma::mat collab_graph;
  if(W.isNull()) {
    collab_graph = arma::mat(nb_agents, nb_agents, arma::fill::ones);
    collab_graph = collab_graph / nb_agents;
    collab_graph.diag() = arma::colvec(nb_agents, arma::fill::zeros);
  }
  else
    collab_graph = Rcpp::as< arma::mat >(W);


  // compute degree (sum each rows)
  arma::colvec degree = arma::sum(collab_graph, 1);

  // compute confidence
  arma::colvec confidence(nb_agents);
  for(int i = 0; i < nb_agents; i++)
    confidence[i] = agents[i]->dim();
  confidence /= sum(confidence);

  // compute alphas
  arma::colvec alpha(nb_agents);
  for(int i = 0; i < nb_agents; i++) {
    alpha[i] = 1 / (1 + mu * confidence[i] * agents[i]->lipschitz());
  }


  // run the algorithm !

  for(int i = 0; i < nb_agents * nb_iter; i++) {
    // check for keyboard interrupt
    R_CheckUserInterrupt();

    // get current agent
    int id = agent_activations[i];
    Agent* curr_agent = agents[id];

    // compute local gradient
    arma::colvec grad = curr_agent->gradient();
    arma::colvec from_others(grad.size(), arma::fill::zeros);
    for(int k = 0; k < nb_agents; k++)
      from_others += collab_graph(id, k) / degree[id] * agents[k]->coefficients();

    arma::colvec new_coefficients =
      (1 - alpha[id]) * curr_agent->coefficients()
      + alpha[id] * (from_others - mu * confidence[id] * grad);

    curr_agent->coefficients(new_coefficients);

    curr_agent->update_history();
  }

  Rcpp::List ret = Rcpp::List::create();
  for(int i = 0; i < nb_agents; i++)
    ret.push_back(agents[i]->to_list());

  return ret;
}
