#include "server.h"

int server_session::connections = 0;


server_session::pointer server_session::create(asio::io_context& io_context, tcp_server* server, int port ) {
  return pointer(new server_session(io_context, server, port));
}

server_session::~server_session() {
  // one session is deleted
  server_pointer_->delete_session();
  // network::nb_server_session--;

  // if no more session is running, stop the server
  // this line should be removed to let the server running after computation ends
  if(server_pointer_->nb_active_session() == 0)
    server_pointer_->terminate();
}


server_session::server_session(asio::io_context& io_context, tcp_server* server, int port)
  : socket_(io_context),
    connection_id_(connections),
    buffer_(std::vector<float>(var::dim)),
    round_timer_(io_context),
    server_pointer_(server) {
  acout(2) << "[serv " << var::id << " -> " << connection_id_ << "] creating new potential session" << std::endl;

  // add one to connections id counter
  connections++;

}

tcp::socket& server_session::socket() {
  return socket_;
}

void server_session::start() {
  //network::nb_server_session++;

  acout(2) << "[serv " << var::id << " -> " << connection_id_ << "] start()" << std::endl;

  // generate welcome message
  message_ = std::vector<float>(1, message_code::welcome);
  acout(1) << "[serv " << var::id << " -> " << connection_id_ << "] sending: " << message_ << std::endl;

  // send
  asio::async_write(socket_, asio::buffer(message_),
			   boost::bind(&server_session::handle_write, shared_from_this(),
				       asio::placeholders::error,
				       asio::placeholders::bytes_transferred,
				       message_code::welcome));
}

void server_session::handle_write(const asio::error_code& error, size_t bytes_transferred, const int& code) {
  acout(2) << "[serv " << var::id << " -> " << connection_id_ << "] handle_write() : " << code << std::endl;

  if (!error) {
    if(code == message_code::welcome){
      // count number of sessions
      server_pointer_->add_session();
    }

    if(code == message_code::vector || code == message_code::ask_vector) {
      var::nb_sent++;

      acout(2) << "[serv " << var::id << " -> " << connection_id_ << "] sent " << var::nb_sent << " vectors"<< std::endl;
    }

    
    do_read();
  }
  else {
    acout(1) << "[serv " << var::id << " -> " << connection_id_ << "] error sending message " << message_ << " : " << error.message() << std::endl;

    //    std::cerr << error.message() << std::endl;
  }
}



void server_session::do_read() {
  acout(2) << "[serv " << var::id << " -> " << connection_id_ << "] do_read()" << std::endl;

 asio::async_read(socket_, asio::buffer(buffer_),
			 asio::transfer_at_least(1),
			 boost::bind(&server_session::handle_read, shared_from_this(),
				     asio::placeholders::error,
				     asio::placeholders::bytes_transferred)
			 );
}

void server_session::handle_read(const asio::error_code& error, size_t number_bytes_read) {
  acout(2) << "[serv " << var::id << " -> " << connection_id_ << "] handle_read(), " << number_bytes_read << " bytes read, with error message " << error.message() << std::endl;

  if (!error && number_bytes_read > 0) {
    int code = floor(buffer_[0] + 0.5);

    if(code == message_code::heartbeat) {
      acout(2) << "[serv " << var::id << " -> " << connection_id_ << "] received heartbeat" << std::endl;
      do_read();
    }
    else {
      do_write(code);
    }
  }

  else if (error == asio::error::eof || number_bytes_read == 0) {
    acout(1) << "[serv " << var::id << " -> " << connection_id_ << "] connection killed by client" << std::endl;

    // cancel timer as it holds a pointer to this session
    // and deleting all pointers will close the server
    round_timer_.cancel();
  }

  else {
    acout(1) << "[serv " << var::id << " -> " << connection_id_ << "] " << error.message() << std::endl;
  }

}

void server_session::do_write(const int& code) {
  acout(2) << "[serv " << var::id << " -> " << connection_id_ << "] do_write()" << std::endl;

  if(code == message_code::ask_vector || code == message_code::ask_cov_matrix) {

    if(!var::serv_vector_ready) {
      wait_for_round(0, code);
    }

    else {
      // retrieve round asked by client
      int asked_round = floor(buffer_[1] + 0.5);

      // if we are in the right round, we can send the vector
      if(asked_round == var::round) {

	// get lock for server vector
	std::unique_lock<std::mutex> lock(mtx::server);
	std::vector<float> vector_to_send = var::serv_vec;

	// format message to send
	message_ = std::vector<float>(vector_to_send.size() + 1);

	if(code == message_code::ask_vector)
	  message_[0] = message_code::vector;
	else
	  message_[0] = message_code::cov_matrix;

	std::copy(vector_to_send.begin(), vector_to_send.end(),
		  message_.begin() + 1);


	acout(1) << "[serv " << var::id << " -> " << connection_id_ << "] sending vector: " << message_ << std::endl;

	asio::async_write(socket_, asio::buffer(message_),
				 boost::bind(&server_session::handle_write, shared_from_this(),
					     asio::placeholders::error,
					     asio::placeholders::bytes_transferred,
					     message_code::vector));
      }
      else {
	acout(1) << "[serv " << var::id << " -> " << connection_id_ << "] asked " << asked_round << " but we have " << var::round << " thus waiting..." <<  std::endl;

	wait_for_round(50);
      }
    }
  }
  else {
    acout(2) << "[serv " << var::id << " -> " << connection_id_ << "] unknown message received from client: " << buffer_ << std::endl;
  }
}


void server_session::wait_for_round(const int& t, const int& code) {
  // try again in 100ms
  round_timer_.expires_from_now(std::chrono::milliseconds(t));

  round_timer_.async_wait(boost::bind(&server_session::do_write, shared_from_this(),
				      code));
}




// TCP SERVER

tcp_server::tcp_server(asio::io_context& io_context, const int& port)
  : io_context_(io_context),
    acceptor_(io_context, tcp::endpoint(tcp::v4(), port)),
    stopped_(false),
    nb_active_session_(0),
    ready_(false) {

  this->port = port;
  start_accept();
}

tcp_server::~tcp_server() {

}

void tcp_server::terminate() {
  acout(2) << "[bigserver " << var::id << "] terminating server" << std::endl;
  
  // closing acceptor and stopping io context will remove all pending operations,
  // therefore gracefully closing the server.
  stopped_ = true;
  acceptor_.close();
  io_context_.stop();
}


void tcp_server::start_accept() {
  acout(2) << "[bigserver " << var::id << "] start_accept()" << std::endl;

  server_session::pointer new_connection =
    server_session::create(io_context_, this, port);


  acceptor_.async_accept(new_connection->socket(),
			 boost::bind(&tcp_server::handle_accept, this, new_connection,
				     asio::placeholders::error));
}

void tcp_server::handle_accept(server_session::pointer new_connection,
			       const asio::error_code& error) {
  acout(2) << "[bigserver " << var::id << "] handle_accept() with error " << error.message() << std::endl;

  if (!error) {
    new_connection->start();
  }
  else {
    acout(2) << "[bigserver " << var::id << "] Error in handle_accept: " << error.message() << std::endl;
  }

  if (!stopped_) 
    start_accept();
}

int tcp_server::nb_active_session() {
  return nb_active_session_;
}

void tcp_server::add_session() {
  nb_active_session_++;
}

void tcp_server::delete_session() {
  nb_active_session_--;
}

bool tcp_server::ready() {
  return ready_;
}

void tcp_server::ready(const bool& r) {
  ready_ = r;
}
