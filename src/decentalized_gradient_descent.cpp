#include "algorithm.h"
#define EPS 1e-7

DecentralizedGradientDescent::DecentralizedGradientDescent(const int& nb_local_iter,
							   const int& nb_iter,
							   LearningRate* learning_rate,
							   const int& batch,
							   const bool& simplify)
  : batch_(batch) {
  nb_iter_ = nb_iter;
  nb_local_iter_ = nb_local_iter;
  simplify_ = simplify;
  learning_rate_ = learning_rate;
}

DecentralizedGradientDescent::~DecentralizedGradientDescent() {

}

void DecentralizedGradientDescent::initialize(Agent* agent) {

}

void DecentralizedGradientDescent::initialize_locally(std::vector< Agent* > agents) {

  for(unsigned int i = 0; i < agents.size(); i++) {

    initialize(agents[i]);

  }

}


void DecentralizedGradientDescent::local_iter(Agent* agent, const int& round) {


  arma::colvec grad = arma::colvec(agent->dim(), arma::fill::zeros);
  arma::colvec update = arma::colvec(agent->dim(), arma::fill::zeros);

    
  // update model
  for(int iter = 0; iter < nb_local_iter_; iter++) {

    // compute local gradient
    grad = agent->gradient(true);

    // update learning rate
    learning_rate_->update(agent, round);
    
    // update coefficients
    agent->coefficients(agent->coefficients() - learning_rate_->get() % grad);


  }


}

void DecentralizedGradientDescent::aggregate_results(Agent* agent,
						     const std::vector< std::vector<float> >& other_coefficients,
						     const arma::colvec& collab) {

  arma::colvec aggregated_coefficients(agent->dim(), arma::fill::zeros);

  for(unsigned int i = 0; i < other_coefficients.size(); i++) {
    arma::colvec coef = arma::conv_to< arma::colvec >::from(other_coefficients[i]);
    aggregated_coefficients += collab[i] * coef;
  }

  agent->coefficients(aggregated_coefficients);
}

void DecentralizedGradientDescent::aggregate_results(Agent* agent,
						     std::vector< Agent* > other_agents,
						     const arma::colvec& collab) {

  arma::colvec aggregated_coefficients(agent->dim(), arma::fill::zeros);

  for(unsigned int i = 0; i < other_agents.size(); i++) {
    aggregated_coefficients += collab[i] * other_agents[i]->coefficients();
  }

  agent->coefficients(aggregated_coefficients);

}
