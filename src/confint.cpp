#include "confint.h"
#include "agent.h"

ConfInt::ConfInt(float confidence)
  : defined_(false),
    confidence_(confidence) {

}

Rcpp::Nullable< Rcpp::List > ConfInt::to_list() {
  if(defined_) {
    return R_NilValue;
  }

  else {
    return Rcpp::List::create(
      Rcpp::Named("lower") = lower_,
      Rcpp::Named("upper") = upper_);
  }
}

void ConfInt::compute_on_network(Agent* agent, Network* network) {
  compute_total_nb_samples(agent, network);

  var::init_round(-1);
  acout(0) << "[main " << var::id << "] beginning round " << var::round << std::endl;

  // compute local matrix
  // here!
  arma::mat covmat = compute_local(agent);

  covmat.reshape(agent->dim() * agent->dim(), 1);

  // store it as a std::vector<float> (from arma::mat)
  {
    std::unique_lock<std::mutex> lock(mtx::server);

    var::serv_vec = arma::conv_to< std::vector<float> >::from(covmat);
    var::serv_vector_ready = true;
  }

  // request vectors from others
  network->request_vectors(var::round, message_code::ask_cov_matrix);


  while(var::nb_sent < network->nb_agents() || var::nb_received < network->nb_agents()) {
    // wait until everything is sent and received
  }

  acout(1) << "[main " << var::id << "] received and sent all : " << var::nb_sent << ", " << var::nb_received << std::endl;


  arma::colvec std_error;
  {
    std::unique_lock<std::mutex> lock(mtx::client);

    // compute standard error from recevied vectors
    std_error = aggregate_stderror(agent, var::client_vec);

  }

  boost::math::students_t dist(total_samples_ - agent->dim());
  double fac = boost::math::quantile(dist, (1.0 - confidence_) / 2);

  lower_ = agent->coefficients() + fac * std_error;
  upper_ = agent->coefficients() - fac * std_error;
}


void ConfInt::compute_total_nb_samples(Agent* agent, Network* network) {
  var::init_round(-2);
  acout(0) << "[main " << var::id << "] beginning round " << var::round << std::endl;

  int nb_samples = agent->X().n_rows;

  // store it as a std::vector<float> (from arma::mat)
  {
    std::unique_lock<std::mutex> lock(mtx::server);

    var::serv_vec = { nb_samples };
    var::serv_vector_ready = true;
  }

  // request vectors from others
  network->request_vectors(var::round, message_code::ask_cov_matrix);

  while(var::nb_sent < network->nb_agents() || var::nb_received < network->nb_agents()) {
    // wait until everything is sent and received
  }

  acout(1) << "[main " << var::id << "] received and sent all : " << var::nb_sent << ", " << var::nb_received << std::endl;


  total_samples_ = 0;
  {
    std::unique_lock<std::mutex> lock(mtx::client);

    for(int i = 0; i < var::client_vec.size(); i++)
      total_samples_ += var::client_vec[i][0];
  }
}

arma::mat ConfInt::compute_local(Agent* agent) {

  arma::colvec coef = agent->coefficients();
  arma::mat X = agent->X();
  arma::colvec Z = X * coef;
  arma::colvec pred = 1.0 / (1.0 + vecexp(-Z));
  // it would be much cleaner to use arma::exp here but it sometime crashes for unexplainable reasons...
  //  for(int i = 0; i < Z.n_elem; i++)
  //    pred[i] = 1.0 / (1.0 + exp(-Z[i]));
  arma::colvec A = pred % (1.0 - pred);

  acout(0) << X.n_rows << "*" << X.n_cols << " /// "
	   << coef.n_elem << " "
	   << Z.n_elem << " "
	   << pred.n_elem << " "
	   << A.n_elem << " " << std::endl;

  arma::mat X2 = X;
  for(int i = 0; i < X.n_rows; i++) {
    X2.row(i) *= A[i];
  }

  arma::mat Xt = X.t();
  acout(0) << Xt.n_rows << "*" << Xt.n_cols << " (" << Xt.n_elem << ") /// "
	   << X2.n_rows << "*" << X2.n_cols << " (" << Xt.n_elem << ")" << std::endl;

  arma::mat res = Xt * X2;
  acout(0) << res.n_rows << "*" << res.n_cols << std::endl;

  return Xt * X2;
}


arma::mat ConfInt::aggregate_cov(Agent* agent, const std::vector< std::vector<float> >& vec) {

  arma::colvec aggregated_mat(vec[0].size(), arma::fill::zeros);

  acout(0) << vec[0] << std::endl;
  for(unsigned int i = 0; i < vec.size(); i++) {
    arma::colvec mat = arma::conv_to< arma::colvec >::from(vec[i]);
    aggregated_mat += mat;
  }

  arma::mat ret = arma::conv_to<arma::mat>::from(aggregated_mat);
  ret.reshape(var::dim, var::dim);

  // return covariance
  return arma::inv(ret);

}

arma::colvec ConfInt::aggregate_stderror(Agent* agent, const std::vector< std::vector< float > >& vec){
  // compute covariance
  arma::mat cov = aggregate_cov(agent, vec);

  // compute and return standard error
  return arma::sqrt(arma::diagvec(cov));

}
