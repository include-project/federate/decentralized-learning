#include "utils.h"

std::vector<int> linspace(int begin, int end, int nb_points) {
  if(nb_points > end - begin + 1)
    nb_points = end - begin + 1;

  std::vector<int> ret(nb_points);
  float step = float(end) / (nb_points - 1);

  for(int i = 0; i < nb_points; i++) {
    ret[i] = step * i + begin;
  }
  ret[nb_points - 1] = end;

  return ret;
}

std::vector< int > generate_activations(const int& nb_agents, const int& nb_iter, const bool& random_activations) {
  std::random_device rd;
  std::mt19937 rng(rd());
  std::uniform_int_distribution<int> uni(0, nb_agents - 1);

  std::vector<int> v(nb_iter * nb_agents);
  for(int i = 0; i < nb_iter * nb_agents; i++) {
    if(random_activations)
      v[i] = uni(rng);
    else
      v[i] = i % nb_agents;
  }

  return v;
}


arma::mat extract_X_from_dataframe (const Rcpp::DataFrame& df,
				    const std::string& label,
				    const bool& intercept) {

  int p = intercept ? df.length() : df.length() - 1;
  int start = intercept ? 1 : 0;

  Rcpp::CharacterVector names = df.names();
  arma::mat out(df.nrows(), p);
  //Rcpp::CharacterVector out_colnames(df.length() - 1);

  int iter = start;
  for (int i = 0; i < df.ncol(); i++) {
    if (names[i] != label) {
      arma::colvec curr = df[i];
      arma::colvec currcol = out.col(iter);
      out.col(iter) = curr;
      //out_colnames[iter] = names[i];
      iter++;
    }
  }

  if(intercept) {
    out.col(0) = arma::colvec(df.nrows(), arma::fill::ones);
  }

  //colnames(out) = out_colnames;
  return out;
}


Rcpp::CharacterVector extract_names_from_dataframe(const Rcpp::DataFrame& df,
						   const std::string& label,
						   const bool& intercept) {

  int p = intercept ? df.length() : df.length() - 1;
  int start = intercept ? 1 : 0;
  Rcpp::CharacterVector names = df.names();

  Rcpp::CharacterVector out(p);

  int iter = start;
  for (int i = 0; i < df.ncol(); i++) {
    if (names[i] != label) {
      out[iter] = names[i];
      iter++;
    }
  }

  if(intercept) {
    out[0] = "(Intercept)";
  }

  return out;
}


int dimension(const Rcpp::DataFrame& df, const bool& intercept) {
  if(intercept)
    return df.length();

  else
    return df.length() - 1;
}
